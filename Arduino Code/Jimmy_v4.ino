//Jimmy the Rover Vers.: 4 for ATmega2560 

//////////////////////////////////
///////////CURRENT TASK//////////
////////////////////////////////
//NONE
//////////////////////////////////
///////////LAST EDIT//////////
////////////////////////////////
//ADD VELOCITY

//Required libraries
#include <Wire.h>
#include <math.h>

const byte interruptPin = 20;
volatile byte state = LOW;

//Motordriver pin allocation (Zuordnung_Arduino_Treiber.xlsx)
int driver[24] = {31,26,44,39,24,42,38,33,29,40,32,22,34,43,45,41,35,36,37,30,23,27,28,25};
int driver_speed[4] = {6,5,4,3};
int start_pins[4] = {39,40,41,27};
int stop_pins[4] = {42,22,36,25};
int release_pins[4] = {24,32,35,28};

//Variables for data input
const byte numChars = 32;       //Character limiter (optional)
char receivedChars[numChars];   //Buffer for received Characters
char tempChars[numChars];       //Temporary buffer for received Characters
boolean newData = false;
boolean auto_run = false;       //Run signal
boolean step_release = false;
boolean running = false;
float angle = 0;
float angle_raw = 0;
float rotation_vel = 0;
int mode = 0;                   //Mode of operation (0 = joystick control, 1 = auto mode)
int pin_nr = 0;
int speed = 128;
int magnitude = 0;


//Variables for vector calculation of stepper speeds
double V1 = 0;                  
double V2 = 0;
double V3 = 0;
double V4 = 0;
double a1 = 0;
double a2 = 0;
double a3 = 0;
double a4 = 0;
double K1 = 0;
double K2 = 0;
double K3 = 0;
double K4 = 0;
double sin_multipl = 0.25;
double speed_multi = 0;

//Variable for turn calculation
double R = 30.48;

//additional variables for joystick control
int x_wert = 0;
int y_wert = 0;
int x_change = 0;
int y_change = 0;
int x_w = 0;
int y_w = 0;


/////////////////////////////////////////////////////////////////////////////////////////
//END OF INITIALIZATION/////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

void setup() 
{ 
  Serial.begin(115200);         //Debug Connection
  Serial1.begin(115200);        //RasPi Connection


//initialize pins

  for (int pin = 0; pin <= 23; pin++)
  {
    pinMode(driver[pin], OUTPUT);
  }

  for (int pin = 0; pin <= 3; pin++)
  {
    pinMode(driver_speed[pin], OUTPUT);
  }
  pinMode(7,INPUT);
  
  for (int i=0; i <=3; i++)
  {
    digitalWrite(release_pins[i], HIGH);
  }
 pinMode(46, INPUT_PULLUP);
 pinMode(interruptPin, INPUT_PULLUP);
 pinMode(49, OUTPUT);
 pinMode(47, OUTPUT);
 pinMode(13, OUTPUT);

 attachInterrupt(digitalPinToInterrupt(interruptPin), Freigabe, FALLING);
 
}

/////////////////////////////////////////////////////////////////////////////////////////
//END OF SETUP//////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

void loop() 
{
  for (int i=0; i <=3; i++)
  {
    digitalWrite(release_pins[i], state);
  }
  digitalWrite(13, state);
  
  mode = digitalRead(46);     //Read out mode switch
  if (mode == 0)              //Joystick mode
  {
    //Serial.println("Manual");
    
    joystick_move();          //Go to joystick control function
  }
  else if (mode == 1)                      //Auto mode
  {    
    //Serial.println("Auto");
    recvWithStartEndMarkers(); 
    if (newData == true)
    {
      strcpy(tempChars, receivedChars);
      parseData();
    }
    if (auto_run == true)
    {  
      
    }

    newData = false;
  }
}


/////////////////////////////////////////////////////////////////////////////////////////
//END OF LOOP///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////
//FUNCTIONS FOR AUTO OPERATION//////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
void recvWithStartEndMarkers() 
{
  static boolean recvInProgress = false;         
  static byte ndx = 0;        //character index of received data
  char startMarker = '<';
  char endMarker = '>';
  char rc;                    //received data
  while (Serial1.available() > 0 && newData == false)  //Only read if new data is available
  {
    rc = Serial1.read();           //Read out serial input and store data in rc
    if (recvInProgress == true)   //Check if start marker was received and data should be stored
    {
      if (rc != endMarker)        //Only store character that come in before end marker
      {
        receivedChars[ndx] = rc;  //Store received data in character arry
        ndx++;
        if (ndx >= numChars)      //Overwrite if character limit reached
        {
          ndx = numChars - 1;
        }
      }
      else 
      {
        receivedChars[ndx] = '\0'; //Terminate the string
        recvInProgress = false;    //End receiving process
        ndx = 0;
        newData = true;           
      }
    }
    else if (rc == startMarker)    //Start receiving process on start marker input
    {
      recvInProgress = true;
    }
  }
}

void parseData()  // split the data into its parts
{      
  char * strtokIndx;   // this is used by strtok() as an index
 
  strtokIndx = strtok(tempChars, ","); 
  angle_raw = atof(strtokIndx);     // convert this part to an integer
  angle = angle_raw *(M_PI/180);

  strtokIndx = strtok(NULL, ",");
  rotation_vel = atof(strtokIndx);  

  strtokIndx = strtok(NULL, ",");
  magnitude = atof(strtokIndx);

  strtokIndx = strtok(NULL, ",");
  auto_run = atoi(strtokIndx);  

  strtokIndx = strtok(NULL, ",");
  step_release = atoi(strtokIndx);  
}

void calc_turn()
{
  K1 = rotation_vel/100;
  K2 = K1;
  K3 = -K1;
  K4 = -K1;
}


/////////////////////////////////////////////////////////////////////////////////////////
//FUNCTIONS FOR JOYSTICK CONTROL////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
void joystick_move()
{
  // if (digitalRead(7) == 0)
  // {
  //   if (running = false)
  //   {
  //     driver_start();
  //     running = true;
  //   }
  //   else
  //   {
  //     driver_stop();
  //     running = false;
  //   }
  // }


//read joystick
  x_wert = analogRead(A14);
  y_wert = analogRead(A15);
//  Serial.print("X");
//  Serial.print(x_wert);
//  Serial.print("Y");
//  Serial.println(y_wert);
  x_change = abs(512-x_wert);
  y_change = abs(512-y_wert);

//check if joystick movement has changed and run or release steppers accordingly

  joystick_compensate();
  magnitude = sqrt(square(x_w)+square(y_w));
  angle = atan2(y_w, x_w);
//  Serial.print("angle: ");
//  Serial.print(angle);
//  Serial.print("    speed ");
//  Serial.println(magnitude);
  if(magnitude != 0)
  {
    set_driver_speed();
    driver_start();
  }
  else
    driver_stop();
    

}

void joystick_compensate()    //compensate bad accuracy of joystick hardware
{
  if (x_wert <= 512)
  {
    x_w = -(1023-x_wert);
    x_w = x_w+512;
  }
  else
  { 
    x_w = x_wert;
    x_w = x_w-512;
  }
  if (y_wert <= 512)
  {
    y_w = -(1023-y_wert);
    y_w = y_w+512;
  }
  else
  {
    y_w = y_wert;
    y_w = y_w-512;
  }
  if (abs(x_w)<18)
    x_w=0;
  if (abs(y_w)<18)
    y_w=0;
  if (x_w>490)
    x_w=511;
  if (y_w>490)
    y_w=512;
  if (x_w<-490)
    x_w=-512;
  if (y_w<-490)
    y_w=-511;    
}


/////////////////////////////////////////////////////////////////////////////////////////
//FUNTIONS FOR Driver CONTROL//////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
int map_speed(int speed_percent)
{
  if (speed_percent > 0)
  {
    speed = map(speed_percent, 1, 100, 128, 255);
  }
  if (speed_percent < 0)
  {
    speed = map(speed_percent, -100, -1, 0, 126);
  }
  if (speed_percent == 0)
  {
    speed = 127;
  }
  return speed;
}
void calc_speed()
{ 
  double max_V = 0;       
  //calculate direction
  a1 = (angle-((sin_multipl)*M_PI));
  a2 = (angle+((sin_multipl)*M_PI));
  a3 = (angle+((sin_multipl)*M_PI));
  a4 = (angle-((sin_multipl)*M_PI));
  V1 = magnitude*sin(a1)+K1;
  V2 = magnitude*sin(a2)+K2;
  V3 = -magnitude*sin(a3)+K3;
  V4 = -magnitude*sin(a4)+K4;
  //find fastest wheel
  max_V = max(abs(V1),abs(V2));
  max_V = max(max_V,abs(V3));
  max_V = max(max_V,abs(V4));
  //normalize to 100
  V1 = (V1/max_V)*100;
  V2 = (V2/max_V)*100;
  V3 = (V3/max_V)*100;
  V4 = (V4/max_V)*100;
}

void set_driver_speed()
{
  //Serial.println("set_driver_speed");
  calc_speed();
  analogWrite(driver_speed[0], map_speed(V1));
  analogWrite(driver_speed[1], map_speed(V2));
  analogWrite(driver_speed[2], map_speed(V3));
  analogWrite(driver_speed[3], map_speed(V4));
//  Serial.print(map_speed(V1));
//  Serial.print(map_speed(V2));
//  Serial.print(map_speed(V3));
//  Serial.println(map_speed(V4));
}

void driver_stop()
{
  for (int i=0; i<=3; i++)
  {
    digitalWrite(start_pins[i], LOW);
    digitalWrite(stop_pins[i], HIGH);  
  }
}

void driver_start()
{
  for (int i=0; i<=3; i++)
  {
    digitalWrite(stop_pins[i], LOW);
    digitalWrite(start_pins[i], HIGH);  
  }
}



void Freigabe() 
{
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > 200) 
  {
    state = !state;
  }
  last_interrupt_time = interrupt_time;
  
}

/////////////////////////////////////////////////////////////////////////////////////////
//END OF CODE///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////
//DEBUG OUTPUTS/////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
