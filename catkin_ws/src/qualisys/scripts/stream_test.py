#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#TODO adapt"""Panda Autodynamics package"""
#TODO DOCSTRING DOCUMENTATION
#TODO autoformat with black and isort
#TODO lint with flake8
import asyncio
import qtm
import xml.etree.ElementTree as ET

async def main():
    #connection = await qtm.connect("127.0.0.1")
    connection = await qtm.connect("192.168.0.157")
    if connection is None:
        print("Failed to connect")
        return
    else:
        print("connected successfully")
        return connection

if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(main())
