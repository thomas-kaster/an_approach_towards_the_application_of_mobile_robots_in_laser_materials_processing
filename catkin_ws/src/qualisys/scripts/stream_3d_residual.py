#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#TODO adapt"""Panda Autodynamics package"""
#TODO DOCSTRING DOCUMENTATION
#TODO autoformat with black and isort
#TODO lint with flake8
import rospy
import asyncio
import qtm
import math
from std_msgs.msg import Float64MultiArray

not_init_first_value = True
first_x_value = 0.0
first_y_value = 0.0

class Measurement:
    def __init__(self):
        
        self.zeit_0 = None
        self.pub_ = rospy.Publisher('qualisys_pub',Float64MultiArray, tcp_nodelay=True, queue_size = 1)
        
        self.msg = Float64MultiArray()
        
        
    def on_packet(self, packet):
        """ Callback function that is called everytime a data packet arrives from QTM """
        #print("Framenumber: {}".format(packet.framenumber))

        global not_init_first_value
        global first_x_value
        global first_y_value

        zeit_neu = packet.timestamp
        if self.zeit_0 == None:
            self.zeit_0 = zeit_neu
        else:
            self.z0zeit_0= min(zeit_neu, self.zeit_0)
        #print("Timestamp: {}".format(packet.timestamp-self.zeit_0))
        header, markers = packet.get_6d_residual()
        #print("Component info: {}".format(header))
        msg = Float64MultiArray()
        '''
        msg_list = []
        for marker in markers:
            pub_data = [marker.x, marker.y, marker.residual]
            if math.isnan(marker.x):
                print("recalibrate")
                rospy.signal_shutdown("recalibration necessary!")
                
            msg_list.append(pub_data)
            #print("\t", pub_data)
        #print(msg_list)
        '''
        
        msg = Float64MultiArray()

        #Done to start from [0,0] Position
        if(not_init_first_value):
            first_x_value = markers[0][0].x
            first_y_value = markers[0][0].y            
            not_init_first_value = False

        pos = markers[0][0]

        msg.data = [pos.x-first_x_value, pos.y-first_y_value, markers[0][2].residual]

        #self.msg.data = msg_list    
        self.pub_.publish(msg)
            

        

    async def setup(self):
        """ Main function """
        #connection = await qtm.connect("127.0.0.1") #connect to host pc from host pc
        #connection = await qtm.connect("134.61.165.175")
        connection = await qtm.connect("192.168.0.157")
        if connection is None:
            print("no connection")
            return
        
        try:
            await connection.take_control('')
        except:
            print("taking control failed")
            return
        else:
            print("took control")
        try:
            await connection.new()
        except:
            print("new connection failed")
            return
        else:
            print("new connection established")
        
        #input("Press Enter to start stream")
        print("stream started")
        
        #await connection.get_current_frame(components=["3d"])
        #first_frame = await connection.get_current_frame(components=["3d"])
        #self.zeit_0 = first_frame.timestamp +10000 #woher kommen die 10000??
        
        
        await connection.stream_frames(components=["6dres"], on_packet=self.on_packet)


if __name__ == "__main__":
    rospy.init_node('qualisys_pub_node',anonymous=True, disable_signals=True)
    a = Measurement()
    asyncio.ensure_future(a.setup())
    asyncio.get_event_loop().run_forever()
    rospy.spin()
