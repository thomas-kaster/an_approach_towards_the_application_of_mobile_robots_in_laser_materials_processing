#!/usr/bin/env python3
import time
from typing import Type

import numpy as np
import rospy
import os
from pyfirmata import Arduino
from std_msgs.msg import Float64, Float64MultiArray


def num_to_range(
    num: Type[Float64],
    inMin: Type[Float64],
    inMax: Type[Float64],
    outMin: Type[Float64],
    outMax: Type[Float64],
) -> Float64:
    """Convert a number range to another range, maintaining ratio

    Args:
        num (Type[Float64]): Number to convert
        inMin (Type[Float64]): Initial minimum range
        inMax (Type[Float64]): Initial maximum range
        outMin (Type[Float64]): New minimum range
        outMax (Type[Float64]): New maximum range

    Returns:
        Float64: Converted number from new range
    """

    return outMin + (float(num - inMin) / float(inMax - inMin) * (outMax - outMin))


def calc_robot_angle(
    current_pos: Type[Float64MultiArray], target_pos: Type[Float64MultiArray]
) -> Float64:
    """Calculate angle of robot which it should use to reach target position from current position

    Args:
        current_pos (Type[Float64MultiArray]): Current position of robot in x- and y-coordinates
        target_pos (Type[Float64MultiArray]): Target position in x- and y-coordinates

    Returns:
        Float64: Return angle of robot
    """

    # x-values identical, but difference in Y values. Determine driving forward or backward
    if current_pos[0] == target_pos[0]:
        if target_pos[1] > current_pos[1]:
            robot_angle = 90.0
        else:
            robot_angle = -90.0

    # y-values identical, but difference in X values. Determine driving left or right
    elif current_pos[1] == target_pos[1]:
        if target_pos[0] > current_pos[0]:
            robot_angle = 0.0
        else:
            robot_angle = 180.0

    # target point above
    elif target_pos[1] > current_pos[1]:
        # target point on left upper
        if current_pos[0] > target_pos[0]:
            robot_angle = (
                np.arctan(
                    abs(target_pos[1] - current_pos[1])
                    / abs(target_pos[0] - current_pos[0])
                )
                * (180.0 / np.pi)
                + 90.0
            )
            robot_angle = num_to_range(robot_angle, 90, 180, 180, 90)

        else:
            robot_angle = np.arctan(
                (target_pos[1] - current_pos[1]) / (target_pos[0] - current_pos[0])
            ) * (180.0 / np.pi)

    # target point below
    else:  # target_pos[1] < current_pos[1]:
        # target point on left side
        if current_pos[0] > target_pos[0]:
            robot_angle = (
                np.arctan(
                    (target_pos[1] - current_pos[1]) / (target_pos[0] - current_pos[0])
                )
                * (180.0 / np.pi)
                - 180.0
            )

        # target point on right side
        else:
            robot_angle = (
                np.arctan(
                    abs(target_pos[1] - current_pos[1])
                    / (target_pos[0] - current_pos[0])
                )
                * (180.0 / np.pi)
                - 90.0
            )
            robot_angle = num_to_range(robot_angle, -90, 0, 0, -90)

    return round(robot_angle, 2)


def calc_travel_time(
    current_pos: Type[Float64MultiArray],
    target_pos: Type[Float64MultiArray],
    robot_velocity: Type[Float64],
) -> Float64:
    """Calculate time which is needed to travel from current position to target position

    Args:
        current_pos (Type[Float64MultiArray]): Current position of robot in x- and y-coordinates
        target_pos (Type[Float64MultiArray]): Target position in x- and y-coordinates
        robot_velocity (Type[Float64]): Velocity which was set at beginning
    Returns:
        Float64: calculated time
    """
    return (
        (
            abs(current_pos[0] - target_pos[0]) ** 2
            + abs(current_pos[1] - target_pos[1]) ** 2
        )
        ** (1 / 2)
    ) / robot_velocity
    # time = route_length / robot_velocity
    # return time


def set_wheel_direction(arr: Type[Float64MultiArray]) -> None:
    """Set wheel rotation direction of each wheel

    Args:
        arr (Type[Float64MultiArray]): array which contains the wheel velocities. Only signs of velocites are relevant
    """
    # Left rear wheel
    if arr[0] >= 0:
        switch1.write(1)
    else:
        switch1.write(0)

    # Right rear wheel
    if arr[1] >= 0:
        switch2.write(0)
    else:
        switch2.write(1)

    # Left front wheel
    if arr[2] >= 0:
        switch3.write(0)
    else:
        switch3.write(1)

    # Right front wheel
    if arr[3] >= 0:
        switch4.write(1)
    else:
        switch4.write(0)


def move(velocity=20, angle=90, drive_time=1):
    # Calculate individual wheel velocities considering arrangement of wheels
    angle_rad = angle * (np.pi / 180)
    offset = np.pi * 0.25
    wheel_vel = [
        -np.sin(angle_rad - offset),
        -np.sin(angle_rad + offset),
        np.sin(angle_rad + offset),
        np.sin(angle_rad - offset),
    ]

    # Determine in which direction wheels should rotate
    set_wheel_direction(wheel_vel)

    # Write duty cycle to each wheel with regards to desired velocity
    max_V = max(map(abs, wheel_vel))
    drives = [drive1, drive2, drive3, drive4]

    for drive, vel in zip(
        drives, wheel_vel
    ):  # drives[0] -> wheel_vel[0]; drives[1] -> wheel_vel[1]; ...
        drive.write(((abs(vel)) / max_V) * (velocity / 200))  # (200mm/s = max velocity)

    # Start driving for TIME [sec] to ANGLE [deg] with VELOCITY [mm/s]
    stop.write(0)
    start.write(1)

    time.sleep(drive_time)

    start.write(0)
    stop.write(1)


def callback():
    # Set parameters robot
    amount_rounds = 1

    if rospy.has_param("~vel"):
        robot_velocity = rospy.get_param("~vel")
    else:
        robot_velocity = 50
    
    # Driving the robot certain amount of rounds over trajectory
    for j in range(amount_rounds):
        # Calculate angle and time between current position and target position
        for i in range(1, len(trajectory)):
            robot_angle = calc_robot_angle(trajectory[i - 1], trajectory[i])
            travel_time = calc_travel_time(
                trajectory[i], trajectory[i - 1], robot_velocity
            )

            # Output on console
            if i > 1:
                print(
                    "\nDriving from position ",
                    trajectory[i - 1],
                    " to ",
                    trajectory[i],
                )
                print("angle: ", round(robot_angle, 2), "deg")

            # Robot moves to angle with velocity and calculated time
            move(robot_velocity, robot_angle, travel_time)

        # Stop roboter after completing trajectory
        move(0, 0, 0)


if __name__ == "__main__":
    # Check correct serial port
    board = Arduino("/dev/ttyACM2")

    # PWM pins for analog speed control
    drive1 = board.get_pin("d:5:p")
    drive2 = board.get_pin("d:6:p")
    drive3 = board.get_pin("d:9:p")
    drive4 = board.get_pin("d:10:p")

    # Switch pins for setting up wheel rotation direction
    switch1 = board.get_pin("d:7:o")
    switch2 = board.get_pin("d:8:o")
    switch3 = board.get_pin("d:2:o")
    switch4 = board.get_pin("d:3:o")

    # Start/Stop pins
    start = board.get_pin("d:11:o")
    stop = board.get_pin("d:4:o")

    # Save positions of csv in a list
    directory = "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/robot_motion/csv/"
    
    if rospy.has_param("~traj"):
        traj_filename = rospy.get_param("~traj")
    else:
        traj_filename = "test.csv"
    
    file_traj = os.path.join(directory,traj_filename)

    print(file_traj)

    with open(
        file_traj,
        newline="",
    ) as csvfile:
        trajectory = np.loadtxt(csvfile, delimiter=",")
    rospy.init_node("trajectory")

    callback()