#!/usr/bin/env python3
import time
import numpy
from pyfirmata import Arduino

# Check correct serial port
board = Arduino("/dev/ttyACM2")  

# PWM pins for analog speed control (0.0 - 1.0 (0%-100%))
drive1 = board.get_pin("d:5:p")
drive2 = board.get_pin("d:6:p")
drive3 = board.get_pin("d:9:p")
drive4 = board.get_pin("d:10:p")

# Switch pins for setting up wheel rotation direction 
switch1 = board.get_pin("d:7:o")
switch2 = board.get_pin("d:8:o")
switch3 = board.get_pin("d:2:o")
switch4 = board.get_pin("d:3:o")

# Start/Stop pins
start = board.get_pin("d:11:o")
stop = board.get_pin("d:4:o")

def set_wheel_direction(arr):
    
    #Left rear wheel
    if (arr[0]>=0):
        switch1.write(1)
    else: switch1.write(0)

    #Right rear wheel
    if (arr[1]>=0):
        switch2.write(0)
    else: switch2.write(1)

    #Left front wheel
    if (arr[2]>=0):
        switch3.write(0)
    else: switch3.write(1)

    #Right front wheel
    if (arr[3]>=0):
        switch4.write(1)
    else: switch4.write(0)

def drive(velocity=10,angle=90,drive_time=1):

    # calculate direction and add turn (0 deg = right; 90 deg = forward; 180 deg = left, 270 deg = backwards)
    V1 = -numpy.sin(angle * (numpy.pi / 180) - ((0.25) * numpy.pi))
    V2 = -numpy.sin(angle * (numpy.pi / 180) + ((0.25) * numpy.pi))
    V3 = numpy.sin(angle * (numpy.pi / 180) + ((0.25) * numpy.pi))
    V4 = numpy.sin(angle * (numpy.pi / 180) - ((0.25) * numpy.pi))

    # determine in which direction wheels should rotate (0 = anti-clockwise; 1 = clockwise)
    set_wheel_direction([V1,V2,V3,V4])

    # normalize to fastest wheel
    max_V = max(abs(V1), abs(V2))
    max_V = max(max_V, abs(V3))
    max_V = max(max_V,abs(V4))

    # write to drivers
    drive1.write((abs(V1)/max_V)*(velocity/200)) #(200mm/s = max velocity)
    drive2.write((abs(V2)/max_V)*(velocity/200))
    drive3.write((abs(V3)/max_V)*(velocity/200))
    drive4.write((abs(V4)/max_V)*(velocity/200))

    #Start driving for TIME [sec] to ANGLE [deg] with VELOCITY [mm/s]
    stop.write(0)
    start.write(1)
    
    time.sleep(drive_time)
    
    start.write(0)
    stop.write(1)

drive()
