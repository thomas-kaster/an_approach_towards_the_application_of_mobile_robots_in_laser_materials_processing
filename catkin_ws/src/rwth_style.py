# RWTH Corporate Design can be found here:
# https://intranet.rwth-aachen.de/en/group/guest/articledetailpage/-/asset_publisher/cadSeXJzMOzq/content/id/21535889

class colors:

    blue        = (0, 84/255, 159/255, 1)   
    black       = (0, 0, 0, 1)
    magenta     = (227/255, 0, 102/255, 1)
    yellow      = (255/255, 237/255, 0, 1)
    petrol      = (0, 97/255, 101/255, 1)
    turquoise   = (0, 152/255, 161/255, 1)
    green       = (87/255, 171/255, 39/255, 1)
    green_2     = (189/255, 205/255, 0, 1)
    orange      = (246/255, 168/255, 0, 1)
    red         = (204/255, 7/255, 30/255, 1)
    bordeaux    = (161/255, 16/255, 53/255, 1)
    violett     = (97/255, 33/255, 88/255, 1)
    lila        = (122/255, 111/255, 172/255, 1)
    

class markers:

    attained_1  = "x"
    attained_2  = "."
    estimated_1 = "^"
    estimated_2 = "o"
    estimated_3 = "s"