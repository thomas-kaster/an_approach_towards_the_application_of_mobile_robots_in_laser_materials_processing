#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#TODO adapt"""Panda Autodynamics package"""
#TODO DOCSTRING DOCUMENTATION
#TODO autoformat with black and isort
#TODO lint with flake8

import signal
import sys
import time
import numpy
import rospy
import serial
import csv

time_tick_list = []


def sensor_initialization(sensor_adr,baud_rate,timeout=None) -> None:
    """Initialize sensor"""
    global sensor
    sensor = serial.Serial(sensor_adr, baud_rate, timeout=None)  # open serial port
    sensor.close() #clear buffer
    sensor.open()


def sensor_readout() -> None:
    global sensor
    global time_tick_list
    rospy.init_node("encoder", anonymous=True)
    sensor_adr  = "/dev/ttyACM4"

    sensor_initialization(sensor_adr,115200)

    while not rospy.is_shutdown():
        
        line = sensor.readline().strip().decode()
        values_string = numpy.array(line.split(";"))
        
        print(values_string)

        #Publishing sensor data
        time = float(values_string[0])
        tick = -float(values_string[1])

        time_tick_list.append([time,tick])


def exit(signal, frame) -> None:
    
    global sensor
    global time_tick_list

    sensor.close()
    sensor.open()
    sensor.close()

    print("Sensor closed")

    with open('/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/encoder_ros/csv/enc_lfw.csv', 'w', encoding='UTF8', newline='') as a:
        writer = csv.writer(a)
        writer.writerows(time_tick_list)

    sys.exit(0)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, exit)
    
    try:
        sensor_readout()
    except rospy.ROSInterruptException:
        pass