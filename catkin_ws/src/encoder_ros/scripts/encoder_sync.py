#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#TODO adapt"""Panda Autodynamics package"""
#TODO DOCSTRING DOCUMENTATION
#TODO autoformat with black and isort
#TODO lint with flake8
import message_filters
import rospy
from typing import Type
from encoder_ros.msg import Encoder
import csv
import numpy as np

class sync(object):
   
    def __init__(self):        
    
        rospy.on_shutdown(self.save_data)

        #Create lists to save data
        self.enc_lfw_list = []
        self.enc_rfw_list = []
        self.enc_lrw_list = []
        self.enc_rrw_list = []
        self.x_y_phiz_list = []

        #Subscribe to each encoder topic of each wheel
        self.enc_lfw_sub = message_filters.Subscriber("/odom_enc_lfw", Encoder)
        self.enc_rfw_sub = message_filters.Subscriber("/odom_enc_rfw", Encoder)
        self.enc_lrw_sub = message_filters.Subscriber("/odom_enc_lrw", Encoder)
        self.enc_rrw_sub = message_filters.Subscriber("/odom_enc_rrw", Encoder)

        #Synchronize encoders
        self.ats = message_filters.ApproximateTimeSynchronizer([self.enc_lfw_sub, self.enc_rfw_sub, self.enc_lrw_sub, self.enc_rrw_sub], queue_size = 1, slop = 0.006) #Slope anpassen
        self.ats.registerCallback(self.syncedCallback)

        #Robot specifications
        self.wheel_radius = 50 #100mm durchmesser
        dist_horiz_wheelaxis_bodycenter = (318.5+50)/2 #318,5+50/2
        dist_vert_wheelaxis_bodycenter = 200 #200mm


        #Define transition matrix for calculation of position with angles
        dist_sum = dist_horiz_wheelaxis_bodycenter + dist_vert_wheelaxis_bodycenter

        self.trans_matrix = np.array([[1, 1, 1, 1],
                                    [1, -1, -1, 1],
                                    [-(1/(dist_sum)), (1/(dist_sum)), -(1/dist_sum), (1/dist_sum)]],
                                    dtype=float)


    def syncedCallback(self, enc_lfw:Type[Encoder], enc_rfw:Type[Encoder], enc_lrw:Type[Encoder], enc_rrw:Type[Encoder])->None:
        
        #Save data
        self.enc_lfw_list.append([enc_lfw.time, enc_lfw.ticks])
        self.enc_rfw_list.append([enc_rfw.time, enc_rfw.ticks])
        self.enc_lrw_list.append([enc_lrw.time, enc_lrw.ticks])
        self.enc_rrw_list.append([enc_rrw.time, enc_rrw.ticks])

        #Calculate angle of each wheel in radians (1 wheel turn = 16000 ticks)
        angle_lfw = (-enc_lfw.ticks / 16000) * (2*np.pi)
        angle_rfw = (enc_rfw.ticks / 16000) * (2*np.pi)
        angle_lrw = (-enc_lrw.ticks / 16000) * (2*np.pi)
        angle_rrw = (enc_rrw.ticks / 16000) * (2*np.pi)

        #Calculate x_pos, y_pos and angle around z-axes
        angles = np.transpose(([angle_lfw, angle_rfw, angle_lrw, angle_rrw]))
        x_y_phiz = (self.wheel_radius/4) * self.trans_matrix.dot(angles)
        
        #Save calculated positions
        x_pos = round(x_y_phiz[0],2)
        y_pos = round(x_y_phiz[1],2)
        phiz = round(x_y_phiz[2],2)
        self.x_y_phiz_list.append([x_pos,y_pos,phiz])


        print(x_pos,y_pos,phiz)
        
    def save_data(self):
        
        #Write down saved data to csv files
        with open('/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/encoder_ros/csv/enc_lfw.csv', 'w', encoding='UTF8', newline='') as a:
            writer = csv.writer(a)
            writer.writerows(self.enc_lfw_list)

        with open('/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/encoder_ros/csv/enc_rfw.csv', 'w', encoding='UTF8', newline='') as b:
            writer = csv.writer(b)
            writer.writerows(self.enc_rfw_list)

        with open('/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/encoder_ros/csv/enc_lrw.csv', 'w', encoding='UTF8', newline='') as c:
            writer = csv.writer(c)
            writer.writerows(self.enc_lrw_list)

        with open('/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/encoder_ros/csv/enc_rrw.csv', 'w', encoding='UTF8', newline='') as d:
            writer = csv.writer(d)
            writer.writerows(self.enc_rrw_list)

        with open('/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/encoder_ros/csv/x_y_phiz.csv', 'w', encoding='UTF8', newline='') as d:
            writer = csv.writer(d)
            writer.writerows(self.x_y_phiz_list)

if __name__ == "__main__":
    rospy.init_node("enc_sync")
    sync_obj = sync()
    rospy.spin()


