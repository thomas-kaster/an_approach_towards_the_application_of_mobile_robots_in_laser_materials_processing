#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#TODO adapt"""Panda Autodynamics package"""
#TODO DOCSTRING DOCUMENTATION
#TODO autoformat with black and isort
#TODO lint with flake8

import signal
import sys
import time
import numpy
import rospy
import serial
import csv
from encoder_ros.msg import Encoder

time_tick_list = []


def sensor_initialization(sensor_adr,baud_rate,timeout=None) -> None:
    """Initialize sensor"""
    global sensor
    sensor = serial.Serial(sensor_adr, baud_rate, timeout=None)  # open serial port
    sensor.close() #clear buffer
    sensor.open()


def sensor_readout() -> None:
    global sensor
    rospy.init_node("encoder", anonymous=True)
    sensor_adr  = rospy.get_param("~address")
    #sensor_adr  = "/dev/ttyACM4"
    pub = rospy.Publisher("odom", Encoder, queue_size=1)
    msg = Encoder()
    rate = rospy.Rate(100)

    sensor_initialization(sensor_adr,115200)

    while not rospy.is_shutdown():
        
        line = sensor.readline().strip().decode()
        values_string = numpy.array(line.split(";"))
        
        #Publishing sensor data
        msg.time = int(values_string[0])
        msg.ticks = int(values_string[1])
        msg.header.stamp = rospy.Time.now()
        pub.publish(msg)
        rate.sleep()


def exit(signal, frame) -> None:
    
    global sensor
    sensor.close()
    sensor.open()
    sensor.close()

    print("Sensor closed")
    sys.exit(0)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, exit)
    
    try:
        sensor_readout()
    except rospy.ROSInterruptException:
        pass