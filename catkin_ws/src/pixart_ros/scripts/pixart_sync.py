#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#TODO adapt"""Panda Autodynamics package"""
#TODO DOCSTRING DOCUMENTATION
#TODO autoformat with black and isort
#TODO lint with flake8
import message_filters
import rospy
from typing import Type
from nav_msgs.msg import Odometry
import csv

class sync(object):
   
    def __init__(self):        
    
        rospy.on_shutdown(self.save_data)

        self.pix180_list = []
        self.pix0_list = []

        self.pix180_sub = message_filters.Subscriber("/odom_pix180", Odometry)
        self.pix0_sub = message_filters.Subscriber("/odom_pix0", Odometry)

        #Synchronize the sensors
        self.ats = message_filters.ApproximateTimeSynchronizer([self.pix180_sub, self.pix0_sub], queue_size = 1, slop = 0.005) #Slope anpassen
        self.ats.registerCallback(self.syncedCallback)

    def syncedCallback(self, pix180:Type[Odometry], pix0:Type[Odometry])->None:
        
        self.pix180_list.append([pix180.pose.pose.position.x, pix180.pose.pose.position.y])
        self.pix0_list.append([pix0.pose.pose.position.x, pix0.pose.pose.position.y])

        
    def save_data(self):
        
        #Write down saved data to csv files
        with open('/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/pixart_ros/csv/pix180.csv', 'w', encoding='UTF8', newline='') as a:
            writer = csv.writer(a)
            writer.writerows(self.pix180_list)

        with open('/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/pixart_ros/csv/pix0.csv', 'w', encoding='UTF8', newline='') as b:
            writer = csv.writer(b)
            writer.writerows(self.pix0_list)

        
if __name__ == "__main__":
    rospy.init_node("pix_sync")
    sync_obj = sync()
    rospy.spin()


