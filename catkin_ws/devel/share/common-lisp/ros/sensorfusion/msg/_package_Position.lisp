(cl:in-package sensorfusion-msg)
(cl:export '(HEADER-VAL
          HEADER
          X-VAL
          X
          Y-VAL
          Y
          X_VAR-VAL
          X_VAR
          Y_VAR-VAL
          Y_VAR
          AZIMUTH-VAL
          AZIMUTH
))