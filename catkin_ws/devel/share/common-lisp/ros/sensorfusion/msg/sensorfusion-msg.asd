
(cl:in-package :asdf)

(defsystem "sensorfusion-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "Motion" :depends-on ("_package_Motion"))
    (:file "_package_Motion" :depends-on ("_package"))
    (:file "Position" :depends-on ("_package_Position"))
    (:file "_package_Position" :depends-on ("_package"))
    (:file "SyncPos" :depends-on ("_package_SyncPos"))
    (:file "_package_SyncPos" :depends-on ("_package"))
  ))