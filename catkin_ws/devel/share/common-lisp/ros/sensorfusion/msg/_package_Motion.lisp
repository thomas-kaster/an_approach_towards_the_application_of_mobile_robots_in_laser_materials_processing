(cl:in-package sensorfusion-msg)
(cl:export '(DT-VAL
          DT
          VELOCITY-VAL
          VELOCITY
          ROT_VEL-VAL
          ROT_VEL
          DIRECTION-VAL
          DIRECTION
          MOVE-VAL
          MOVE
))