(cl:in-package sensorfusion-srv)
(cl:export '(PATH_NUMBER-VAL
          PATH_NUMBER
          SEG_NUMBER-VAL
          SEG_NUMBER
          VELOCITY-VAL
          VELOCITY
          ANGLE-VAL
          ANGLE
          DT-VAL
          DT
          DIRECTION-VAL
          DIRECTION
          SEG_DT-VAL
          SEG_DT
          SEG_ANGLE-VAL
          SEG_ANGLE
))