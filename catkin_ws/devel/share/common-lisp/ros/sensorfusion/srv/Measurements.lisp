; Auto-generated. Do not edit!


(cl:in-package sensorfusion-srv)


;//! \htmlinclude Measurements-request.msg.html

(cl:defclass <Measurements-request> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass Measurements-request (<Measurements-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Measurements-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Measurements-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name sensorfusion-srv:<Measurements-request> is deprecated: use sensorfusion-srv:Measurements-request instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Measurements-request>) ostream)
  "Serializes a message object of type '<Measurements-request>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Measurements-request>) istream)
  "Deserializes a message object of type '<Measurements-request>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Measurements-request>)))
  "Returns string type for a service object of type '<Measurements-request>"
  "sensorfusion/MeasurementsRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Measurements-request)))
  "Returns string type for a service object of type 'Measurements-request"
  "sensorfusion/MeasurementsRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Measurements-request>)))
  "Returns md5sum for a message object of type '<Measurements-request>"
  "6413914098d25bfa9253dc3582c7fffc")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Measurements-request)))
  "Returns md5sum for a message object of type 'Measurements-request"
  "6413914098d25bfa9253dc3582c7fffc")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Measurements-request>)))
  "Returns full string definition for message of type '<Measurements-request>"
  (cl:format cl:nil "#Ask only for measurement data~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Measurements-request)))
  "Returns full string definition for message of type 'Measurements-request"
  (cl:format cl:nil "#Ask only for measurement data~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Measurements-request>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Measurements-request>))
  "Converts a ROS message object to a list"
  (cl:list 'Measurements-request
))
;//! \htmlinclude Measurements-response.msg.html

(cl:defclass <Measurements-response> (roslisp-msg-protocol:ros-message)
  ((measure
    :reader measure
    :initarg :measure
    :type sensorfusion-msg:SyncPos
    :initform (cl:make-instance 'sensorfusion-msg:SyncPos)))
)

(cl:defclass Measurements-response (<Measurements-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Measurements-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Measurements-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name sensorfusion-srv:<Measurements-response> is deprecated: use sensorfusion-srv:Measurements-response instead.")))

(cl:ensure-generic-function 'measure-val :lambda-list '(m))
(cl:defmethod measure-val ((m <Measurements-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader sensorfusion-srv:measure-val is deprecated.  Use sensorfusion-srv:measure instead.")
  (measure m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Measurements-response>) ostream)
  "Serializes a message object of type '<Measurements-response>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'measure) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Measurements-response>) istream)
  "Deserializes a message object of type '<Measurements-response>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'measure) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Measurements-response>)))
  "Returns string type for a service object of type '<Measurements-response>"
  "sensorfusion/MeasurementsResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Measurements-response)))
  "Returns string type for a service object of type 'Measurements-response"
  "sensorfusion/MeasurementsResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Measurements-response>)))
  "Returns md5sum for a message object of type '<Measurements-response>"
  "6413914098d25bfa9253dc3582c7fffc")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Measurements-response)))
  "Returns md5sum for a message object of type 'Measurements-response"
  "6413914098d25bfa9253dc3582c7fffc")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Measurements-response>)))
  "Returns full string definition for message of type '<Measurements-response>"
  (cl:format cl:nil "#Measurement data~%SyncPos measure~%~%================================================================================~%MSG: sensorfusion/SyncPos~%std_msgs/Header header~%~%#Lidar ~%float64 x_pos_lid~%float64 y_pos_lid~%float64 x_var_lid~%float64 y_var_lid~%float64 azimuth_lid~%float64 azimuth_var_lid~%~%~%#Pixart Front ~%float64 x_pos_vpix~%float64 y_pos_vpix~%float64 x_var_vpix~%float64 y_var_vpix~%~%~%#Pixart Back~%float64 x_pos_hpix~%float64 y_pos_hpix~%float64 x_var_hpix~%float64 y_var_hpix~%~%~%#Qualisys~%float64 x_pos_qual~%float64 y_pos_qual~%float64 x_var_qual~%float64 y_var_qual~%~%# #IMU~%float64 azimuth_imu~%float64 azimuth_var_imu~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Measurements-response)))
  "Returns full string definition for message of type 'Measurements-response"
  (cl:format cl:nil "#Measurement data~%SyncPos measure~%~%================================================================================~%MSG: sensorfusion/SyncPos~%std_msgs/Header header~%~%#Lidar ~%float64 x_pos_lid~%float64 y_pos_lid~%float64 x_var_lid~%float64 y_var_lid~%float64 azimuth_lid~%float64 azimuth_var_lid~%~%~%#Pixart Front ~%float64 x_pos_vpix~%float64 y_pos_vpix~%float64 x_var_vpix~%float64 y_var_vpix~%~%~%#Pixart Back~%float64 x_pos_hpix~%float64 y_pos_hpix~%float64 x_var_hpix~%float64 y_var_hpix~%~%~%#Qualisys~%float64 x_pos_qual~%float64 y_pos_qual~%float64 x_var_qual~%float64 y_var_qual~%~%# #IMU~%float64 azimuth_imu~%float64 azimuth_var_imu~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Measurements-response>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'measure))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Measurements-response>))
  "Converts a ROS message object to a list"
  (cl:list 'Measurements-response
    (cl:cons ':measure (measure msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'Measurements)))
  'Measurements-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'Measurements)))
  'Measurements-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Measurements)))
  "Returns string type for a service object of type '<Measurements>"
  "sensorfusion/Measurements")