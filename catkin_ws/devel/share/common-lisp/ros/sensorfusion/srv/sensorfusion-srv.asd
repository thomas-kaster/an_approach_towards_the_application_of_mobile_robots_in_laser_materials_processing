
(cl:in-package :asdf)

(defsystem "sensorfusion-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :sensorfusion-msg
)
  :components ((:file "_package")
    (:file "Measurements" :depends-on ("_package_Measurements"))
    (:file "_package_Measurements" :depends-on ("_package"))
    (:file "Trajectory" :depends-on ("_package_Trajectory"))
    (:file "_package_Trajectory" :depends-on ("_package"))
  ))