; Auto-generated. Do not edit!


(cl:in-package pixart_ros-srv)


;//! \htmlinclude PAA5102_service-request.msg.html

(cl:defclass <PAA5102_service-request> (roslisp-msg-protocol:ros-message)
  ((call
    :reader call
    :initarg :call
    :type cl:fixnum
    :initform 0))
)

(cl:defclass PAA5102_service-request (<PAA5102_service-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PAA5102_service-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PAA5102_service-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name pixart_ros-srv:<PAA5102_service-request> is deprecated: use pixart_ros-srv:PAA5102_service-request instead.")))

(cl:ensure-generic-function 'call-val :lambda-list '(m))
(cl:defmethod call-val ((m <PAA5102_service-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pixart_ros-srv:call-val is deprecated.  Use pixart_ros-srv:call instead.")
  (call m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PAA5102_service-request>) ostream)
  "Serializes a message object of type '<PAA5102_service-request>"
  (cl:let* ((signed (cl:slot-value msg 'call)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PAA5102_service-request>) istream)
  "Deserializes a message object of type '<PAA5102_service-request>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'call) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PAA5102_service-request>)))
  "Returns string type for a service object of type '<PAA5102_service-request>"
  "pixart_ros/PAA5102_serviceRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PAA5102_service-request)))
  "Returns string type for a service object of type 'PAA5102_service-request"
  "pixart_ros/PAA5102_serviceRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PAA5102_service-request>)))
  "Returns md5sum for a message object of type '<PAA5102_service-request>"
  "fbde5b207b989cea796b6d575a6902d0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PAA5102_service-request)))
  "Returns md5sum for a message object of type 'PAA5102_service-request"
  "fbde5b207b989cea796b6d575a6902d0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PAA5102_service-request>)))
  "Returns full string definition for message of type '<PAA5102_service-request>"
  (cl:format cl:nil "int8 call~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PAA5102_service-request)))
  "Returns full string definition for message of type 'PAA5102_service-request"
  (cl:format cl:nil "int8 call~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PAA5102_service-request>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PAA5102_service-request>))
  "Converts a ROS message object to a list"
  (cl:list 'PAA5102_service-request
    (cl:cons ':call (call msg))
))
;//! \htmlinclude PAA5102_service-response.msg.html

(cl:defclass <PAA5102_service-response> (roslisp-msg-protocol:ros-message)
  ((values
    :reader values
    :initarg :values
    :type (cl:vector cl:float)
   :initform (cl:make-array 2 :element-type 'cl:float :initial-element 0.0)))
)

(cl:defclass PAA5102_service-response (<PAA5102_service-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PAA5102_service-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PAA5102_service-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name pixart_ros-srv:<PAA5102_service-response> is deprecated: use pixart_ros-srv:PAA5102_service-response instead.")))

(cl:ensure-generic-function 'values-val :lambda-list '(m))
(cl:defmethod values-val ((m <PAA5102_service-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pixart_ros-srv:values-val is deprecated.  Use pixart_ros-srv:values instead.")
  (values m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PAA5102_service-response>) ostream)
  "Serializes a message object of type '<PAA5102_service-response>"
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-single-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)))
   (cl:slot-value msg 'values))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PAA5102_service-response>) istream)
  "Deserializes a message object of type '<PAA5102_service-response>"
  (cl:setf (cl:slot-value msg 'values) (cl:make-array 2))
  (cl:let ((vals (cl:slot-value msg 'values)))
    (cl:dotimes (i 2)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-single-float-bits bits)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PAA5102_service-response>)))
  "Returns string type for a service object of type '<PAA5102_service-response>"
  "pixart_ros/PAA5102_serviceResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PAA5102_service-response)))
  "Returns string type for a service object of type 'PAA5102_service-response"
  "pixart_ros/PAA5102_serviceResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PAA5102_service-response>)))
  "Returns md5sum for a message object of type '<PAA5102_service-response>"
  "fbde5b207b989cea796b6d575a6902d0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PAA5102_service-response)))
  "Returns md5sum for a message object of type 'PAA5102_service-response"
  "fbde5b207b989cea796b6d575a6902d0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PAA5102_service-response>)))
  "Returns full string definition for message of type '<PAA5102_service-response>"
  (cl:format cl:nil "float32[2] values~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PAA5102_service-response)))
  "Returns full string definition for message of type 'PAA5102_service-response"
  (cl:format cl:nil "float32[2] values~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PAA5102_service-response>))
  (cl:+ 0
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'values) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PAA5102_service-response>))
  "Converts a ROS message object to a list"
  (cl:list 'PAA5102_service-response
    (cl:cons ':values (values msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'PAA5102_service)))
  'PAA5102_service-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'PAA5102_service)))
  'PAA5102_service-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PAA5102_service)))
  "Returns string type for a service object of type '<PAA5102_service>"
  "pixart_ros/PAA5102_service")