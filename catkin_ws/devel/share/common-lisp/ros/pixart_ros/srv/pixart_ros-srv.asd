
(cl:in-package :asdf)

(defsystem "pixart_ros-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "PAA5102_service" :depends-on ("_package_PAA5102_service"))
    (:file "_package_PAA5102_service" :depends-on ("_package"))
  ))