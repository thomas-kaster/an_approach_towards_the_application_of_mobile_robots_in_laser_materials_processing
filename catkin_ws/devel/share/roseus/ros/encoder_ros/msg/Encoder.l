;; Auto-generated. Do not edit!


(when (boundp 'encoder_ros::Encoder)
  (if (not (find-package "ENCODER_ROS"))
    (make-package "ENCODER_ROS"))
  (shadow 'Encoder (find-package "ENCODER_ROS")))
(unless (find-package "ENCODER_ROS::ENCODER")
  (make-package "ENCODER_ROS::ENCODER"))

(in-package "ROS")
;;//! \htmlinclude Encoder.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass encoder_ros::Encoder
  :super ros::object
  :slots (_time _ticks _header ))

(defmethod encoder_ros::Encoder
  (:init
   (&key
    ((:time __time) 0)
    ((:ticks __ticks) 0)
    ((:header __header) (instance std_msgs::Header :init))
    )
   (send-super :init)
   (setq _time (round __time))
   (setq _ticks (round __ticks))
   (setq _header __header)
   self)
  (:time
   (&optional __time)
   (if __time (setq _time __time)) _time)
  (:ticks
   (&optional __ticks)
   (if __ticks (setq _ticks __ticks)) _ticks)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:serialization-length
   ()
   (+
    ;; int64 _time
    8
    ;; int64 _ticks
    8
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int64 _time
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _time (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _time) (= (length (_time . bv)) 2)) ;; bignum
              (write-long (ash (elt (_time . bv) 0) 0) s)
              (write-long (ash (elt (_time . bv) 1) -1) s))
             ((and (class _time) (= (length (_time . bv)) 1)) ;; big1
              (write-long (elt (_time . bv) 0) s)
              (write-long (if (>= _time 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _time s)(write-long (if (>= _time 0) 0 #xffffffff) s)))
     ;; int64 _ticks
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _ticks (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _ticks) (= (length (_ticks . bv)) 2)) ;; bignum
              (write-long (ash (elt (_ticks . bv) 0) 0) s)
              (write-long (ash (elt (_ticks . bv) 1) -1) s))
             ((and (class _ticks) (= (length (_ticks . bv)) 1)) ;; big1
              (write-long (elt (_ticks . bv) 0) s)
              (write-long (if (>= _ticks 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _ticks s)(write-long (if (>= _ticks 0) 0 #xffffffff) s)))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int64 _time
#+(or :alpha :irix6 :x86_64)
      (setf _time (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _time (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; int64 _ticks
#+(or :alpha :irix6 :x86_64)
      (setf _ticks (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _ticks (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;;
   self)
  )

(setf (get encoder_ros::Encoder :md5sum-) "3ef374eb7f0d9366b2be1e78b03583ef")
(setf (get encoder_ros::Encoder :datatype-) "encoder_ros/Encoder")
(setf (get encoder_ros::Encoder :definition-)
      "int64 time
int64 ticks
std_msgs/Header header

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :encoder_ros/Encoder "3ef374eb7f0d9366b2be1e78b03583ef")


