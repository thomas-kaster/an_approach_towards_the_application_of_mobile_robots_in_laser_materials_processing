;; Auto-generated. Do not edit!


(when (boundp 'pixart_ros::PAA5102_service)
  (if (not (find-package "PIXART_ROS"))
    (make-package "PIXART_ROS"))
  (shadow 'PAA5102_service (find-package "PIXART_ROS")))
(unless (find-package "PIXART_ROS::PAA5102_SERVICE")
  (make-package "PIXART_ROS::PAA5102_SERVICE"))
(unless (find-package "PIXART_ROS::PAA5102_SERVICEREQUEST")
  (make-package "PIXART_ROS::PAA5102_SERVICEREQUEST"))
(unless (find-package "PIXART_ROS::PAA5102_SERVICERESPONSE")
  (make-package "PIXART_ROS::PAA5102_SERVICERESPONSE"))

(in-package "ROS")





(defclass pixart_ros::PAA5102_serviceRequest
  :super ros::object
  :slots (_call ))

(defmethod pixart_ros::PAA5102_serviceRequest
  (:init
   (&key
    ((:call __call) 0)
    )
   (send-super :init)
   (setq _call (round __call))
   self)
  (:call
   (&optional __call)
   (if __call (setq _call __call)) _call)
  (:serialization-length
   ()
   (+
    ;; int8 _call
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int8 _call
       (write-byte _call s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int8 _call
     (setq _call (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> _call 127) (setq _call (- _call 256)))
   ;;
   self)
  )

(defclass pixart_ros::PAA5102_serviceResponse
  :super ros::object
  :slots (_values ))

(defmethod pixart_ros::PAA5102_serviceResponse
  (:init
   (&key
    ((:values __values) (make-array 2 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _values __values)
   self)
  (:values
   (&optional __values)
   (if __values (setq _values __values)) _values)
  (:serialization-length
   ()
   (+
    ;; float32[2] _values
    (* 4    2)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32[2] _values
     (dotimes (i 2)
       (sys::poke (elt _values i) (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32[2] _values
   (dotimes (i (length _values))
     (setf (elt _values i) (sys::peek buf ptr- :float)) (incf ptr- 4)
     )
   ;;
   self)
  )

(defclass pixart_ros::PAA5102_service
  :super ros::object
  :slots ())

(setf (get pixart_ros::PAA5102_service :md5sum-) "fbde5b207b989cea796b6d575a6902d0")
(setf (get pixart_ros::PAA5102_service :datatype-) "pixart_ros/PAA5102_service")
(setf (get pixart_ros::PAA5102_service :request) pixart_ros::PAA5102_serviceRequest)
(setf (get pixart_ros::PAA5102_service :response) pixart_ros::PAA5102_serviceResponse)

(defmethod pixart_ros::PAA5102_serviceRequest
  (:response () (instance pixart_ros::PAA5102_serviceResponse :init)))

(setf (get pixart_ros::PAA5102_serviceRequest :md5sum-) "fbde5b207b989cea796b6d575a6902d0")
(setf (get pixart_ros::PAA5102_serviceRequest :datatype-) "pixart_ros/PAA5102_serviceRequest")
(setf (get pixart_ros::PAA5102_serviceRequest :definition-)
      "int8 call
---
float32[2] values

")

(setf (get pixart_ros::PAA5102_serviceResponse :md5sum-) "fbde5b207b989cea796b6d575a6902d0")
(setf (get pixart_ros::PAA5102_serviceResponse :datatype-) "pixart_ros/PAA5102_serviceResponse")
(setf (get pixart_ros::PAA5102_serviceResponse :definition-)
      "int8 call
---
float32[2] values

")



(provide :pixart_ros/PAA5102_service "fbde5b207b989cea796b6d575a6902d0")


