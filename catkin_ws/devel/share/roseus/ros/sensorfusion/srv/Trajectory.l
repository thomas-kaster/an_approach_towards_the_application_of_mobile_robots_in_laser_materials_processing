;; Auto-generated. Do not edit!


(when (boundp 'sensorfusion::Trajectory)
  (if (not (find-package "SENSORFUSION"))
    (make-package "SENSORFUSION"))
  (shadow 'Trajectory (find-package "SENSORFUSION")))
(unless (find-package "SENSORFUSION::TRAJECTORY")
  (make-package "SENSORFUSION::TRAJECTORY"))
(unless (find-package "SENSORFUSION::TRAJECTORYREQUEST")
  (make-package "SENSORFUSION::TRAJECTORYREQUEST"))
(unless (find-package "SENSORFUSION::TRAJECTORYRESPONSE")
  (make-package "SENSORFUSION::TRAJECTORYRESPONSE"))

(in-package "ROS")





(defclass sensorfusion::TrajectoryRequest
  :super ros::object
  :slots (_path_number _seg_number _velocity ))

(defmethod sensorfusion::TrajectoryRequest
  (:init
   (&key
    ((:path_number __path_number) 0)
    ((:seg_number __seg_number) 0)
    ((:velocity __velocity) 0.0)
    )
   (send-super :init)
   (setq _path_number (round __path_number))
   (setq _seg_number (round __seg_number))
   (setq _velocity (float __velocity))
   self)
  (:path_number
   (&optional __path_number)
   (if __path_number (setq _path_number __path_number)) _path_number)
  (:seg_number
   (&optional __seg_number)
   (if __seg_number (setq _seg_number __seg_number)) _seg_number)
  (:velocity
   (&optional __velocity)
   (if __velocity (setq _velocity __velocity)) _velocity)
  (:serialization-length
   ()
   (+
    ;; int64 _path_number
    8
    ;; int64 _seg_number
    8
    ;; float64 _velocity
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int64 _path_number
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _path_number (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _path_number) (= (length (_path_number . bv)) 2)) ;; bignum
              (write-long (ash (elt (_path_number . bv) 0) 0) s)
              (write-long (ash (elt (_path_number . bv) 1) -1) s))
             ((and (class _path_number) (= (length (_path_number . bv)) 1)) ;; big1
              (write-long (elt (_path_number . bv) 0) s)
              (write-long (if (>= _path_number 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _path_number s)(write-long (if (>= _path_number 0) 0 #xffffffff) s)))
     ;; int64 _seg_number
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _seg_number (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _seg_number) (= (length (_seg_number . bv)) 2)) ;; bignum
              (write-long (ash (elt (_seg_number . bv) 0) 0) s)
              (write-long (ash (elt (_seg_number . bv) 1) -1) s))
             ((and (class _seg_number) (= (length (_seg_number . bv)) 1)) ;; big1
              (write-long (elt (_seg_number . bv) 0) s)
              (write-long (if (>= _seg_number 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _seg_number s)(write-long (if (>= _seg_number 0) 0 #xffffffff) s)))
     ;; float64 _velocity
       (sys::poke _velocity (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int64 _path_number
#+(or :alpha :irix6 :x86_64)
      (setf _path_number (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _path_number (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; int64 _seg_number
#+(or :alpha :irix6 :x86_64)
      (setf _seg_number (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _seg_number (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; float64 _velocity
     (setq _velocity (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(defclass sensorfusion::TrajectoryResponse
  :super ros::object
  :slots (_angle _dt _direction _seg_dt _seg_angle ))

(defmethod sensorfusion::TrajectoryResponse
  (:init
   (&key
    ((:angle __angle) 0.0)
    ((:dt __dt) 0.0)
    ((:direction __direction) 0.0)
    ((:seg_dt __seg_dt) 0.0)
    ((:seg_angle __seg_angle) 0.0)
    )
   (send-super :init)
   (setq _angle (float __angle))
   (setq _dt (float __dt))
   (setq _direction (float __direction))
   (setq _seg_dt (float __seg_dt))
   (setq _seg_angle (float __seg_angle))
   self)
  (:angle
   (&optional __angle)
   (if __angle (setq _angle __angle)) _angle)
  (:dt
   (&optional __dt)
   (if __dt (setq _dt __dt)) _dt)
  (:direction
   (&optional __direction)
   (if __direction (setq _direction __direction)) _direction)
  (:seg_dt
   (&optional __seg_dt)
   (if __seg_dt (setq _seg_dt __seg_dt)) _seg_dt)
  (:seg_angle
   (&optional __seg_angle)
   (if __seg_angle (setq _seg_angle __seg_angle)) _seg_angle)
  (:serialization-length
   ()
   (+
    ;; float64 _angle
    8
    ;; float64 _dt
    8
    ;; float64 _direction
    8
    ;; float64 _seg_dt
    8
    ;; float64 _seg_angle
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _angle
       (sys::poke _angle (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _dt
       (sys::poke _dt (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _direction
       (sys::poke _direction (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _seg_dt
       (sys::poke _seg_dt (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _seg_angle
       (sys::poke _seg_angle (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _angle
     (setq _angle (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _dt
     (setq _dt (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _direction
     (setq _direction (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _seg_dt
     (setq _seg_dt (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _seg_angle
     (setq _seg_angle (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(defclass sensorfusion::Trajectory
  :super ros::object
  :slots ())

(setf (get sensorfusion::Trajectory :md5sum-) "ed5bcf930c479d7b46b85b6b7dafa672")
(setf (get sensorfusion::Trajectory :datatype-) "sensorfusion/Trajectory")
(setf (get sensorfusion::Trajectory :request) sensorfusion::TrajectoryRequest)
(setf (get sensorfusion::Trajectory :response) sensorfusion::TrajectoryResponse)

(defmethod sensorfusion::TrajectoryRequest
  (:response () (instance sensorfusion::TrajectoryResponse :init)))

(setf (get sensorfusion::TrajectoryRequest :md5sum-) "ed5bcf930c479d7b46b85b6b7dafa672")
(setf (get sensorfusion::TrajectoryRequest :datatype-) "sensorfusion/TrajectoryRequest")
(setf (get sensorfusion::TrajectoryRequest :definition-)
      "#Index of path (mini segments)
int64 path_number

#Index of segment (parts of trajectory)
int64 seg_number

#Linear velocity
float64 velocity
---
#Angle at which robot is heading
float64 angle

#Time to drive path
float64 dt

#Direction at which robot needs to drive
float64 direction

#Segment time to drive
float64 seg_dt

#Segment angle
float64 seg_angle
")

(setf (get sensorfusion::TrajectoryResponse :md5sum-) "ed5bcf930c479d7b46b85b6b7dafa672")
(setf (get sensorfusion::TrajectoryResponse :datatype-) "sensorfusion/TrajectoryResponse")
(setf (get sensorfusion::TrajectoryResponse :definition-)
      "#Index of path (mini segments)
int64 path_number

#Index of segment (parts of trajectory)
int64 seg_number

#Linear velocity
float64 velocity
---
#Angle at which robot is heading
float64 angle

#Time to drive path
float64 dt

#Direction at which robot needs to drive
float64 direction

#Segment time to drive
float64 seg_dt

#Segment angle
float64 seg_angle
")



(provide :sensorfusion/Trajectory "ed5bcf930c479d7b46b85b6b7dafa672")


