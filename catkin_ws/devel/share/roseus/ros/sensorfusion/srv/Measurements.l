;; Auto-generated. Do not edit!


(when (boundp 'sensorfusion::Measurements)
  (if (not (find-package "SENSORFUSION"))
    (make-package "SENSORFUSION"))
  (shadow 'Measurements (find-package "SENSORFUSION")))
(unless (find-package "SENSORFUSION::MEASUREMENTS")
  (make-package "SENSORFUSION::MEASUREMENTS"))
(unless (find-package "SENSORFUSION::MEASUREMENTSREQUEST")
  (make-package "SENSORFUSION::MEASUREMENTSREQUEST"))
(unless (find-package "SENSORFUSION::MEASUREMENTSRESPONSE")
  (make-package "SENSORFUSION::MEASUREMENTSRESPONSE"))

(in-package "ROS")





(defclass sensorfusion::MeasurementsRequest
  :super ros::object
  :slots ())

(defmethod sensorfusion::MeasurementsRequest
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(defclass sensorfusion::MeasurementsResponse
  :super ros::object
  :slots (_measure ))

(defmethod sensorfusion::MeasurementsResponse
  (:init
   (&key
    ((:measure __measure) (instance sensorfusion::SyncPos :init))
    )
   (send-super :init)
   (setq _measure __measure)
   self)
  (:measure
   (&rest __measure)
   (if (keywordp (car __measure))
       (send* _measure __measure)
     (progn
       (if __measure (setq _measure (car __measure)))
       _measure)))
  (:serialization-length
   ()
   (+
    ;; sensorfusion/SyncPos _measure
    (send _measure :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; sensorfusion/SyncPos _measure
       (send _measure :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; sensorfusion/SyncPos _measure
     (send _measure :deserialize buf ptr-) (incf ptr- (send _measure :serialization-length))
   ;;
   self)
  )

(defclass sensorfusion::Measurements
  :super ros::object
  :slots ())

(setf (get sensorfusion::Measurements :md5sum-) "6413914098d25bfa9253dc3582c7fffc")
(setf (get sensorfusion::Measurements :datatype-) "sensorfusion/Measurements")
(setf (get sensorfusion::Measurements :request) sensorfusion::MeasurementsRequest)
(setf (get sensorfusion::Measurements :response) sensorfusion::MeasurementsResponse)

(defmethod sensorfusion::MeasurementsRequest
  (:response () (instance sensorfusion::MeasurementsResponse :init)))

(setf (get sensorfusion::MeasurementsRequest :md5sum-) "6413914098d25bfa9253dc3582c7fffc")
(setf (get sensorfusion::MeasurementsRequest :datatype-) "sensorfusion/MeasurementsRequest")
(setf (get sensorfusion::MeasurementsRequest :definition-)
      "#Ask only for measurement data
---
#Measurement data
SyncPos measure

================================================================================
MSG: sensorfusion/SyncPos
std_msgs/Header header

#Lidar 
float64 x_pos_lid
float64 y_pos_lid
float64 x_var_lid
float64 y_var_lid
float64 azimuth_lid
float64 azimuth_var_lid


#Pixart Front 
float64 x_pos_vpix
float64 y_pos_vpix
float64 x_var_vpix
float64 y_var_vpix


#Pixart Back
float64 x_pos_hpix
float64 y_pos_hpix
float64 x_var_hpix
float64 y_var_hpix


#Qualisys
float64 x_pos_qual
float64 y_pos_qual
float64 x_var_qual
float64 y_var_qual

# #IMU
float64 azimuth_imu
float64 azimuth_var_imu

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id
")

(setf (get sensorfusion::MeasurementsResponse :md5sum-) "6413914098d25bfa9253dc3582c7fffc")
(setf (get sensorfusion::MeasurementsResponse :datatype-) "sensorfusion/MeasurementsResponse")
(setf (get sensorfusion::MeasurementsResponse :definition-)
      "#Ask only for measurement data
---
#Measurement data
SyncPos measure

================================================================================
MSG: sensorfusion/SyncPos
std_msgs/Header header

#Lidar 
float64 x_pos_lid
float64 y_pos_lid
float64 x_var_lid
float64 y_var_lid
float64 azimuth_lid
float64 azimuth_var_lid


#Pixart Front 
float64 x_pos_vpix
float64 y_pos_vpix
float64 x_var_vpix
float64 y_var_vpix


#Pixart Back
float64 x_pos_hpix
float64 y_pos_hpix
float64 x_var_hpix
float64 y_var_hpix


#Qualisys
float64 x_pos_qual
float64 y_pos_qual
float64 x_var_qual
float64 y_var_qual

# #IMU
float64 azimuth_imu
float64 azimuth_var_imu

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id
")



(provide :sensorfusion/Measurements "6413914098d25bfa9253dc3582c7fffc")


