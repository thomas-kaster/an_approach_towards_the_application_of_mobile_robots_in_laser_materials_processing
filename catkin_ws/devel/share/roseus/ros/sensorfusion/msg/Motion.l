;; Auto-generated. Do not edit!


(when (boundp 'sensorfusion::Motion)
  (if (not (find-package "SENSORFUSION"))
    (make-package "SENSORFUSION"))
  (shadow 'Motion (find-package "SENSORFUSION")))
(unless (find-package "SENSORFUSION::MOTION")
  (make-package "SENSORFUSION::MOTION"))

(in-package "ROS")
;;//! \htmlinclude Motion.msg.html


(defclass sensorfusion::Motion
  :super ros::object
  :slots (_dt _velocity _rot_vel _direction _move ))

(defmethod sensorfusion::Motion
  (:init
   (&key
    ((:dt __dt) 0.0)
    ((:velocity __velocity) 0.0)
    ((:rot_vel __rot_vel) 0.0)
    ((:direction __direction) 0.0)
    ((:move __move) 0.0)
    )
   (send-super :init)
   (setq _dt (float __dt))
   (setq _velocity (float __velocity))
   (setq _rot_vel (float __rot_vel))
   (setq _direction (float __direction))
   (setq _move (float __move))
   self)
  (:dt
   (&optional __dt)
   (if __dt (setq _dt __dt)) _dt)
  (:velocity
   (&optional __velocity)
   (if __velocity (setq _velocity __velocity)) _velocity)
  (:rot_vel
   (&optional __rot_vel)
   (if __rot_vel (setq _rot_vel __rot_vel)) _rot_vel)
  (:direction
   (&optional __direction)
   (if __direction (setq _direction __direction)) _direction)
  (:move
   (&optional __move)
   (if __move (setq _move __move)) _move)
  (:serialization-length
   ()
   (+
    ;; float64 _dt
    8
    ;; float64 _velocity
    8
    ;; float64 _rot_vel
    8
    ;; float64 _direction
    8
    ;; float64 _move
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _dt
       (sys::poke _dt (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _velocity
       (sys::poke _velocity (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _rot_vel
       (sys::poke _rot_vel (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _direction
       (sys::poke _direction (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _move
       (sys::poke _move (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _dt
     (setq _dt (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _velocity
     (setq _velocity (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _rot_vel
     (setq _rot_vel (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _direction
     (setq _direction (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _move
     (setq _move (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get sensorfusion::Motion :md5sum-) "672599596dff1ce4af974a3b00ddaedb")
(setf (get sensorfusion::Motion :datatype-) "sensorfusion/Motion")
(setf (get sensorfusion::Motion :definition-)
      "#Time to travel each segment (in s)
float64 dt

#Linear Velocity (0-100 mm/s)
float64 velocity

#Rotation velocity (?)
float64 rot_vel

#Direction robot needs to head (forward = 90, left = 0, right = 180, back = 270)
float64 direction

#Command to let robot start(1)/stop(0)
float64 move
")



(provide :sensorfusion/Motion "672599596dff1ce4af974a3b00ddaedb")


