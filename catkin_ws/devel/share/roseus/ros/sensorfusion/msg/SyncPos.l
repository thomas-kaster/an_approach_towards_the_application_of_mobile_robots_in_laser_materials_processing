;; Auto-generated. Do not edit!


(when (boundp 'sensorfusion::SyncPos)
  (if (not (find-package "SENSORFUSION"))
    (make-package "SENSORFUSION"))
  (shadow 'SyncPos (find-package "SENSORFUSION")))
(unless (find-package "SENSORFUSION::SYNCPOS")
  (make-package "SENSORFUSION::SYNCPOS"))

(in-package "ROS")
;;//! \htmlinclude SyncPos.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass sensorfusion::SyncPos
  :super ros::object
  :slots (_header _x_pos_lid _y_pos_lid _x_var_lid _y_var_lid _azimuth_lid _azimuth_var_lid _x_pos_vpix _y_pos_vpix _x_var_vpix _y_var_vpix _x_pos_hpix _y_pos_hpix _x_var_hpix _y_var_hpix _x_pos_qual _y_pos_qual _x_var_qual _y_var_qual _azimuth_imu _azimuth_var_imu ))

(defmethod sensorfusion::SyncPos
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:x_pos_lid __x_pos_lid) 0.0)
    ((:y_pos_lid __y_pos_lid) 0.0)
    ((:x_var_lid __x_var_lid) 0.0)
    ((:y_var_lid __y_var_lid) 0.0)
    ((:azimuth_lid __azimuth_lid) 0.0)
    ((:azimuth_var_lid __azimuth_var_lid) 0.0)
    ((:x_pos_vpix __x_pos_vpix) 0.0)
    ((:y_pos_vpix __y_pos_vpix) 0.0)
    ((:x_var_vpix __x_var_vpix) 0.0)
    ((:y_var_vpix __y_var_vpix) 0.0)
    ((:x_pos_hpix __x_pos_hpix) 0.0)
    ((:y_pos_hpix __y_pos_hpix) 0.0)
    ((:x_var_hpix __x_var_hpix) 0.0)
    ((:y_var_hpix __y_var_hpix) 0.0)
    ((:x_pos_qual __x_pos_qual) 0.0)
    ((:y_pos_qual __y_pos_qual) 0.0)
    ((:x_var_qual __x_var_qual) 0.0)
    ((:y_var_qual __y_var_qual) 0.0)
    ((:azimuth_imu __azimuth_imu) 0.0)
    ((:azimuth_var_imu __azimuth_var_imu) 0.0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _x_pos_lid (float __x_pos_lid))
   (setq _y_pos_lid (float __y_pos_lid))
   (setq _x_var_lid (float __x_var_lid))
   (setq _y_var_lid (float __y_var_lid))
   (setq _azimuth_lid (float __azimuth_lid))
   (setq _azimuth_var_lid (float __azimuth_var_lid))
   (setq _x_pos_vpix (float __x_pos_vpix))
   (setq _y_pos_vpix (float __y_pos_vpix))
   (setq _x_var_vpix (float __x_var_vpix))
   (setq _y_var_vpix (float __y_var_vpix))
   (setq _x_pos_hpix (float __x_pos_hpix))
   (setq _y_pos_hpix (float __y_pos_hpix))
   (setq _x_var_hpix (float __x_var_hpix))
   (setq _y_var_hpix (float __y_var_hpix))
   (setq _x_pos_qual (float __x_pos_qual))
   (setq _y_pos_qual (float __y_pos_qual))
   (setq _x_var_qual (float __x_var_qual))
   (setq _y_var_qual (float __y_var_qual))
   (setq _azimuth_imu (float __azimuth_imu))
   (setq _azimuth_var_imu (float __azimuth_var_imu))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:x_pos_lid
   (&optional __x_pos_lid)
   (if __x_pos_lid (setq _x_pos_lid __x_pos_lid)) _x_pos_lid)
  (:y_pos_lid
   (&optional __y_pos_lid)
   (if __y_pos_lid (setq _y_pos_lid __y_pos_lid)) _y_pos_lid)
  (:x_var_lid
   (&optional __x_var_lid)
   (if __x_var_lid (setq _x_var_lid __x_var_lid)) _x_var_lid)
  (:y_var_lid
   (&optional __y_var_lid)
   (if __y_var_lid (setq _y_var_lid __y_var_lid)) _y_var_lid)
  (:azimuth_lid
   (&optional __azimuth_lid)
   (if __azimuth_lid (setq _azimuth_lid __azimuth_lid)) _azimuth_lid)
  (:azimuth_var_lid
   (&optional __azimuth_var_lid)
   (if __azimuth_var_lid (setq _azimuth_var_lid __azimuth_var_lid)) _azimuth_var_lid)
  (:x_pos_vpix
   (&optional __x_pos_vpix)
   (if __x_pos_vpix (setq _x_pos_vpix __x_pos_vpix)) _x_pos_vpix)
  (:y_pos_vpix
   (&optional __y_pos_vpix)
   (if __y_pos_vpix (setq _y_pos_vpix __y_pos_vpix)) _y_pos_vpix)
  (:x_var_vpix
   (&optional __x_var_vpix)
   (if __x_var_vpix (setq _x_var_vpix __x_var_vpix)) _x_var_vpix)
  (:y_var_vpix
   (&optional __y_var_vpix)
   (if __y_var_vpix (setq _y_var_vpix __y_var_vpix)) _y_var_vpix)
  (:x_pos_hpix
   (&optional __x_pos_hpix)
   (if __x_pos_hpix (setq _x_pos_hpix __x_pos_hpix)) _x_pos_hpix)
  (:y_pos_hpix
   (&optional __y_pos_hpix)
   (if __y_pos_hpix (setq _y_pos_hpix __y_pos_hpix)) _y_pos_hpix)
  (:x_var_hpix
   (&optional __x_var_hpix)
   (if __x_var_hpix (setq _x_var_hpix __x_var_hpix)) _x_var_hpix)
  (:y_var_hpix
   (&optional __y_var_hpix)
   (if __y_var_hpix (setq _y_var_hpix __y_var_hpix)) _y_var_hpix)
  (:x_pos_qual
   (&optional __x_pos_qual)
   (if __x_pos_qual (setq _x_pos_qual __x_pos_qual)) _x_pos_qual)
  (:y_pos_qual
   (&optional __y_pos_qual)
   (if __y_pos_qual (setq _y_pos_qual __y_pos_qual)) _y_pos_qual)
  (:x_var_qual
   (&optional __x_var_qual)
   (if __x_var_qual (setq _x_var_qual __x_var_qual)) _x_var_qual)
  (:y_var_qual
   (&optional __y_var_qual)
   (if __y_var_qual (setq _y_var_qual __y_var_qual)) _y_var_qual)
  (:azimuth_imu
   (&optional __azimuth_imu)
   (if __azimuth_imu (setq _azimuth_imu __azimuth_imu)) _azimuth_imu)
  (:azimuth_var_imu
   (&optional __azimuth_var_imu)
   (if __azimuth_var_imu (setq _azimuth_var_imu __azimuth_var_imu)) _azimuth_var_imu)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; float64 _x_pos_lid
    8
    ;; float64 _y_pos_lid
    8
    ;; float64 _x_var_lid
    8
    ;; float64 _y_var_lid
    8
    ;; float64 _azimuth_lid
    8
    ;; float64 _azimuth_var_lid
    8
    ;; float64 _x_pos_vpix
    8
    ;; float64 _y_pos_vpix
    8
    ;; float64 _x_var_vpix
    8
    ;; float64 _y_var_vpix
    8
    ;; float64 _x_pos_hpix
    8
    ;; float64 _y_pos_hpix
    8
    ;; float64 _x_var_hpix
    8
    ;; float64 _y_var_hpix
    8
    ;; float64 _x_pos_qual
    8
    ;; float64 _y_pos_qual
    8
    ;; float64 _x_var_qual
    8
    ;; float64 _y_var_qual
    8
    ;; float64 _azimuth_imu
    8
    ;; float64 _azimuth_var_imu
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; float64 _x_pos_lid
       (sys::poke _x_pos_lid (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _y_pos_lid
       (sys::poke _y_pos_lid (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _x_var_lid
       (sys::poke _x_var_lid (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _y_var_lid
       (sys::poke _y_var_lid (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _azimuth_lid
       (sys::poke _azimuth_lid (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _azimuth_var_lid
       (sys::poke _azimuth_var_lid (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _x_pos_vpix
       (sys::poke _x_pos_vpix (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _y_pos_vpix
       (sys::poke _y_pos_vpix (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _x_var_vpix
       (sys::poke _x_var_vpix (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _y_var_vpix
       (sys::poke _y_var_vpix (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _x_pos_hpix
       (sys::poke _x_pos_hpix (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _y_pos_hpix
       (sys::poke _y_pos_hpix (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _x_var_hpix
       (sys::poke _x_var_hpix (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _y_var_hpix
       (sys::poke _y_var_hpix (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _x_pos_qual
       (sys::poke _x_pos_qual (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _y_pos_qual
       (sys::poke _y_pos_qual (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _x_var_qual
       (sys::poke _x_var_qual (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _y_var_qual
       (sys::poke _y_var_qual (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _azimuth_imu
       (sys::poke _azimuth_imu (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _azimuth_var_imu
       (sys::poke _azimuth_var_imu (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; float64 _x_pos_lid
     (setq _x_pos_lid (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _y_pos_lid
     (setq _y_pos_lid (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _x_var_lid
     (setq _x_var_lid (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _y_var_lid
     (setq _y_var_lid (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _azimuth_lid
     (setq _azimuth_lid (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _azimuth_var_lid
     (setq _azimuth_var_lid (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _x_pos_vpix
     (setq _x_pos_vpix (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _y_pos_vpix
     (setq _y_pos_vpix (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _x_var_vpix
     (setq _x_var_vpix (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _y_var_vpix
     (setq _y_var_vpix (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _x_pos_hpix
     (setq _x_pos_hpix (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _y_pos_hpix
     (setq _y_pos_hpix (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _x_var_hpix
     (setq _x_var_hpix (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _y_var_hpix
     (setq _y_var_hpix (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _x_pos_qual
     (setq _x_pos_qual (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _y_pos_qual
     (setq _y_pos_qual (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _x_var_qual
     (setq _x_var_qual (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _y_var_qual
     (setq _y_var_qual (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _azimuth_imu
     (setq _azimuth_imu (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _azimuth_var_imu
     (setq _azimuth_var_imu (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get sensorfusion::SyncPos :md5sum-) "bd785aded9f8b709afab99f1d06bbf54")
(setf (get sensorfusion::SyncPos :datatype-) "sensorfusion/SyncPos")
(setf (get sensorfusion::SyncPos :definition-)
      "std_msgs/Header header

#Lidar 
float64 x_pos_lid
float64 y_pos_lid
float64 x_var_lid
float64 y_var_lid
float64 azimuth_lid
float64 azimuth_var_lid


#Pixart Front 
float64 x_pos_vpix
float64 y_pos_vpix
float64 x_var_vpix
float64 y_var_vpix


#Pixart Back
float64 x_pos_hpix
float64 y_pos_hpix
float64 x_var_hpix
float64 y_var_hpix


#Qualisys
float64 x_pos_qual
float64 y_pos_qual
float64 x_var_qual
float64 y_var_qual

# #IMU
float64 azimuth_imu
float64 azimuth_var_imu

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :sensorfusion/SyncPos "bd785aded9f8b709afab99f1d06bbf54")


