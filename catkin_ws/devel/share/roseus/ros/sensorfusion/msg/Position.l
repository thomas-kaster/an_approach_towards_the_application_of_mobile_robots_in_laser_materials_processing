;; Auto-generated. Do not edit!


(when (boundp 'sensorfusion::Position)
  (if (not (find-package "SENSORFUSION"))
    (make-package "SENSORFUSION"))
  (shadow 'Position (find-package "SENSORFUSION")))
(unless (find-package "SENSORFUSION::POSITION")
  (make-package "SENSORFUSION::POSITION"))

(in-package "ROS")
;;//! \htmlinclude Position.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass sensorfusion::Position
  :super ros::object
  :slots (_header _x _y _x_var _y_var _azimuth ))

(defmethod sensorfusion::Position
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:x __x) 0.0)
    ((:y __y) 0.0)
    ((:x_var __x_var) 0.0)
    ((:y_var __y_var) 0.0)
    ((:azimuth __azimuth) 0.0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _x (float __x))
   (setq _y (float __y))
   (setq _x_var (float __x_var))
   (setq _y_var (float __y_var))
   (setq _azimuth (float __azimuth))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:x
   (&optional __x)
   (if __x (setq _x __x)) _x)
  (:y
   (&optional __y)
   (if __y (setq _y __y)) _y)
  (:x_var
   (&optional __x_var)
   (if __x_var (setq _x_var __x_var)) _x_var)
  (:y_var
   (&optional __y_var)
   (if __y_var (setq _y_var __y_var)) _y_var)
  (:azimuth
   (&optional __azimuth)
   (if __azimuth (setq _azimuth __azimuth)) _azimuth)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; float64 _x
    8
    ;; float64 _y
    8
    ;; float64 _x_var
    8
    ;; float64 _y_var
    8
    ;; float64 _azimuth
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; float64 _x
       (sys::poke _x (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _y
       (sys::poke _y (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _x_var
       (sys::poke _x_var (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _y_var
       (sys::poke _y_var (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _azimuth
       (sys::poke _azimuth (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; float64 _x
     (setq _x (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _y
     (setq _y (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _x_var
     (setq _x_var (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _y_var
     (setq _y_var (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _azimuth
     (setq _azimuth (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get sensorfusion::Position :md5sum-) "add81a928822091058cbded64cb136bc")
(setf (get sensorfusion::Position :datatype-) "sensorfusion/Position")
(setf (get sensorfusion::Position :definition-)
      "#header for Timestamp
std_msgs/Header header

#X-Position
float64 x

#Y-Position
float64 y

#X-Variance
float64 x_var

#Y-Variance
float64 y_var

#Azimuth angle
float64 azimuth
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :sensorfusion/Position "add81a928822091058cbded64cb136bc")


