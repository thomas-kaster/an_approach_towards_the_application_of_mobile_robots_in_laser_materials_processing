// Auto-generated. Do not edit!

// (in-package sensorfusion.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class SyncPos {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.x_pos_lid = null;
      this.y_pos_lid = null;
      this.x_var_lid = null;
      this.y_var_lid = null;
      this.azimuth_lid = null;
      this.azimuth_var_lid = null;
      this.x_pos_vpix = null;
      this.y_pos_vpix = null;
      this.x_var_vpix = null;
      this.y_var_vpix = null;
      this.x_pos_hpix = null;
      this.y_pos_hpix = null;
      this.x_var_hpix = null;
      this.y_var_hpix = null;
      this.x_pos_qual = null;
      this.y_pos_qual = null;
      this.x_var_qual = null;
      this.y_var_qual = null;
      this.azimuth_imu = null;
      this.azimuth_var_imu = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('x_pos_lid')) {
        this.x_pos_lid = initObj.x_pos_lid
      }
      else {
        this.x_pos_lid = 0.0;
      }
      if (initObj.hasOwnProperty('y_pos_lid')) {
        this.y_pos_lid = initObj.y_pos_lid
      }
      else {
        this.y_pos_lid = 0.0;
      }
      if (initObj.hasOwnProperty('x_var_lid')) {
        this.x_var_lid = initObj.x_var_lid
      }
      else {
        this.x_var_lid = 0.0;
      }
      if (initObj.hasOwnProperty('y_var_lid')) {
        this.y_var_lid = initObj.y_var_lid
      }
      else {
        this.y_var_lid = 0.0;
      }
      if (initObj.hasOwnProperty('azimuth_lid')) {
        this.azimuth_lid = initObj.azimuth_lid
      }
      else {
        this.azimuth_lid = 0.0;
      }
      if (initObj.hasOwnProperty('azimuth_var_lid')) {
        this.azimuth_var_lid = initObj.azimuth_var_lid
      }
      else {
        this.azimuth_var_lid = 0.0;
      }
      if (initObj.hasOwnProperty('x_pos_vpix')) {
        this.x_pos_vpix = initObj.x_pos_vpix
      }
      else {
        this.x_pos_vpix = 0.0;
      }
      if (initObj.hasOwnProperty('y_pos_vpix')) {
        this.y_pos_vpix = initObj.y_pos_vpix
      }
      else {
        this.y_pos_vpix = 0.0;
      }
      if (initObj.hasOwnProperty('x_var_vpix')) {
        this.x_var_vpix = initObj.x_var_vpix
      }
      else {
        this.x_var_vpix = 0.0;
      }
      if (initObj.hasOwnProperty('y_var_vpix')) {
        this.y_var_vpix = initObj.y_var_vpix
      }
      else {
        this.y_var_vpix = 0.0;
      }
      if (initObj.hasOwnProperty('x_pos_hpix')) {
        this.x_pos_hpix = initObj.x_pos_hpix
      }
      else {
        this.x_pos_hpix = 0.0;
      }
      if (initObj.hasOwnProperty('y_pos_hpix')) {
        this.y_pos_hpix = initObj.y_pos_hpix
      }
      else {
        this.y_pos_hpix = 0.0;
      }
      if (initObj.hasOwnProperty('x_var_hpix')) {
        this.x_var_hpix = initObj.x_var_hpix
      }
      else {
        this.x_var_hpix = 0.0;
      }
      if (initObj.hasOwnProperty('y_var_hpix')) {
        this.y_var_hpix = initObj.y_var_hpix
      }
      else {
        this.y_var_hpix = 0.0;
      }
      if (initObj.hasOwnProperty('x_pos_qual')) {
        this.x_pos_qual = initObj.x_pos_qual
      }
      else {
        this.x_pos_qual = 0.0;
      }
      if (initObj.hasOwnProperty('y_pos_qual')) {
        this.y_pos_qual = initObj.y_pos_qual
      }
      else {
        this.y_pos_qual = 0.0;
      }
      if (initObj.hasOwnProperty('x_var_qual')) {
        this.x_var_qual = initObj.x_var_qual
      }
      else {
        this.x_var_qual = 0.0;
      }
      if (initObj.hasOwnProperty('y_var_qual')) {
        this.y_var_qual = initObj.y_var_qual
      }
      else {
        this.y_var_qual = 0.0;
      }
      if (initObj.hasOwnProperty('azimuth_imu')) {
        this.azimuth_imu = initObj.azimuth_imu
      }
      else {
        this.azimuth_imu = 0.0;
      }
      if (initObj.hasOwnProperty('azimuth_var_imu')) {
        this.azimuth_var_imu = initObj.azimuth_var_imu
      }
      else {
        this.azimuth_var_imu = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type SyncPos
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [x_pos_lid]
    bufferOffset = _serializer.float64(obj.x_pos_lid, buffer, bufferOffset);
    // Serialize message field [y_pos_lid]
    bufferOffset = _serializer.float64(obj.y_pos_lid, buffer, bufferOffset);
    // Serialize message field [x_var_lid]
    bufferOffset = _serializer.float64(obj.x_var_lid, buffer, bufferOffset);
    // Serialize message field [y_var_lid]
    bufferOffset = _serializer.float64(obj.y_var_lid, buffer, bufferOffset);
    // Serialize message field [azimuth_lid]
    bufferOffset = _serializer.float64(obj.azimuth_lid, buffer, bufferOffset);
    // Serialize message field [azimuth_var_lid]
    bufferOffset = _serializer.float64(obj.azimuth_var_lid, buffer, bufferOffset);
    // Serialize message field [x_pos_vpix]
    bufferOffset = _serializer.float64(obj.x_pos_vpix, buffer, bufferOffset);
    // Serialize message field [y_pos_vpix]
    bufferOffset = _serializer.float64(obj.y_pos_vpix, buffer, bufferOffset);
    // Serialize message field [x_var_vpix]
    bufferOffset = _serializer.float64(obj.x_var_vpix, buffer, bufferOffset);
    // Serialize message field [y_var_vpix]
    bufferOffset = _serializer.float64(obj.y_var_vpix, buffer, bufferOffset);
    // Serialize message field [x_pos_hpix]
    bufferOffset = _serializer.float64(obj.x_pos_hpix, buffer, bufferOffset);
    // Serialize message field [y_pos_hpix]
    bufferOffset = _serializer.float64(obj.y_pos_hpix, buffer, bufferOffset);
    // Serialize message field [x_var_hpix]
    bufferOffset = _serializer.float64(obj.x_var_hpix, buffer, bufferOffset);
    // Serialize message field [y_var_hpix]
    bufferOffset = _serializer.float64(obj.y_var_hpix, buffer, bufferOffset);
    // Serialize message field [x_pos_qual]
    bufferOffset = _serializer.float64(obj.x_pos_qual, buffer, bufferOffset);
    // Serialize message field [y_pos_qual]
    bufferOffset = _serializer.float64(obj.y_pos_qual, buffer, bufferOffset);
    // Serialize message field [x_var_qual]
    bufferOffset = _serializer.float64(obj.x_var_qual, buffer, bufferOffset);
    // Serialize message field [y_var_qual]
    bufferOffset = _serializer.float64(obj.y_var_qual, buffer, bufferOffset);
    // Serialize message field [azimuth_imu]
    bufferOffset = _serializer.float64(obj.azimuth_imu, buffer, bufferOffset);
    // Serialize message field [azimuth_var_imu]
    bufferOffset = _serializer.float64(obj.azimuth_var_imu, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type SyncPos
    let len;
    let data = new SyncPos(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [x_pos_lid]
    data.x_pos_lid = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [y_pos_lid]
    data.y_pos_lid = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [x_var_lid]
    data.x_var_lid = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [y_var_lid]
    data.y_var_lid = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [azimuth_lid]
    data.azimuth_lid = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [azimuth_var_lid]
    data.azimuth_var_lid = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [x_pos_vpix]
    data.x_pos_vpix = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [y_pos_vpix]
    data.y_pos_vpix = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [x_var_vpix]
    data.x_var_vpix = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [y_var_vpix]
    data.y_var_vpix = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [x_pos_hpix]
    data.x_pos_hpix = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [y_pos_hpix]
    data.y_pos_hpix = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [x_var_hpix]
    data.x_var_hpix = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [y_var_hpix]
    data.y_var_hpix = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [x_pos_qual]
    data.x_pos_qual = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [y_pos_qual]
    data.y_pos_qual = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [x_var_qual]
    data.x_var_qual = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [y_var_qual]
    data.y_var_qual = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [azimuth_imu]
    data.azimuth_imu = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [azimuth_var_imu]
    data.azimuth_var_imu = _deserializer.float64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 160;
  }

  static datatype() {
    // Returns string type for a message object
    return 'sensorfusion/SyncPos';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'bd785aded9f8b709afab99f1d06bbf54';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    std_msgs/Header header
    
    #Lidar 
    float64 x_pos_lid
    float64 y_pos_lid
    float64 x_var_lid
    float64 y_var_lid
    float64 azimuth_lid
    float64 azimuth_var_lid
    
    
    #Pixart Front 
    float64 x_pos_vpix
    float64 y_pos_vpix
    float64 x_var_vpix
    float64 y_var_vpix
    
    
    #Pixart Back
    float64 x_pos_hpix
    float64 y_pos_hpix
    float64 x_var_hpix
    float64 y_var_hpix
    
    
    #Qualisys
    float64 x_pos_qual
    float64 y_pos_qual
    float64 x_var_qual
    float64 y_var_qual
    
    # #IMU
    float64 azimuth_imu
    float64 azimuth_var_imu
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new SyncPos(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.x_pos_lid !== undefined) {
      resolved.x_pos_lid = msg.x_pos_lid;
    }
    else {
      resolved.x_pos_lid = 0.0
    }

    if (msg.y_pos_lid !== undefined) {
      resolved.y_pos_lid = msg.y_pos_lid;
    }
    else {
      resolved.y_pos_lid = 0.0
    }

    if (msg.x_var_lid !== undefined) {
      resolved.x_var_lid = msg.x_var_lid;
    }
    else {
      resolved.x_var_lid = 0.0
    }

    if (msg.y_var_lid !== undefined) {
      resolved.y_var_lid = msg.y_var_lid;
    }
    else {
      resolved.y_var_lid = 0.0
    }

    if (msg.azimuth_lid !== undefined) {
      resolved.azimuth_lid = msg.azimuth_lid;
    }
    else {
      resolved.azimuth_lid = 0.0
    }

    if (msg.azimuth_var_lid !== undefined) {
      resolved.azimuth_var_lid = msg.azimuth_var_lid;
    }
    else {
      resolved.azimuth_var_lid = 0.0
    }

    if (msg.x_pos_vpix !== undefined) {
      resolved.x_pos_vpix = msg.x_pos_vpix;
    }
    else {
      resolved.x_pos_vpix = 0.0
    }

    if (msg.y_pos_vpix !== undefined) {
      resolved.y_pos_vpix = msg.y_pos_vpix;
    }
    else {
      resolved.y_pos_vpix = 0.0
    }

    if (msg.x_var_vpix !== undefined) {
      resolved.x_var_vpix = msg.x_var_vpix;
    }
    else {
      resolved.x_var_vpix = 0.0
    }

    if (msg.y_var_vpix !== undefined) {
      resolved.y_var_vpix = msg.y_var_vpix;
    }
    else {
      resolved.y_var_vpix = 0.0
    }

    if (msg.x_pos_hpix !== undefined) {
      resolved.x_pos_hpix = msg.x_pos_hpix;
    }
    else {
      resolved.x_pos_hpix = 0.0
    }

    if (msg.y_pos_hpix !== undefined) {
      resolved.y_pos_hpix = msg.y_pos_hpix;
    }
    else {
      resolved.y_pos_hpix = 0.0
    }

    if (msg.x_var_hpix !== undefined) {
      resolved.x_var_hpix = msg.x_var_hpix;
    }
    else {
      resolved.x_var_hpix = 0.0
    }

    if (msg.y_var_hpix !== undefined) {
      resolved.y_var_hpix = msg.y_var_hpix;
    }
    else {
      resolved.y_var_hpix = 0.0
    }

    if (msg.x_pos_qual !== undefined) {
      resolved.x_pos_qual = msg.x_pos_qual;
    }
    else {
      resolved.x_pos_qual = 0.0
    }

    if (msg.y_pos_qual !== undefined) {
      resolved.y_pos_qual = msg.y_pos_qual;
    }
    else {
      resolved.y_pos_qual = 0.0
    }

    if (msg.x_var_qual !== undefined) {
      resolved.x_var_qual = msg.x_var_qual;
    }
    else {
      resolved.x_var_qual = 0.0
    }

    if (msg.y_var_qual !== undefined) {
      resolved.y_var_qual = msg.y_var_qual;
    }
    else {
      resolved.y_var_qual = 0.0
    }

    if (msg.azimuth_imu !== undefined) {
      resolved.azimuth_imu = msg.azimuth_imu;
    }
    else {
      resolved.azimuth_imu = 0.0
    }

    if (msg.azimuth_var_imu !== undefined) {
      resolved.azimuth_var_imu = msg.azimuth_var_imu;
    }
    else {
      resolved.azimuth_var_imu = 0.0
    }

    return resolved;
    }
};

module.exports = SyncPos;
