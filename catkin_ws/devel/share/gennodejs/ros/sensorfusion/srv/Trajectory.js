// Auto-generated. Do not edit!

// (in-package sensorfusion.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class TrajectoryRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.path_number = null;
      this.seg_number = null;
      this.velocity = null;
    }
    else {
      if (initObj.hasOwnProperty('path_number')) {
        this.path_number = initObj.path_number
      }
      else {
        this.path_number = 0;
      }
      if (initObj.hasOwnProperty('seg_number')) {
        this.seg_number = initObj.seg_number
      }
      else {
        this.seg_number = 0;
      }
      if (initObj.hasOwnProperty('velocity')) {
        this.velocity = initObj.velocity
      }
      else {
        this.velocity = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type TrajectoryRequest
    // Serialize message field [path_number]
    bufferOffset = _serializer.int64(obj.path_number, buffer, bufferOffset);
    // Serialize message field [seg_number]
    bufferOffset = _serializer.int64(obj.seg_number, buffer, bufferOffset);
    // Serialize message field [velocity]
    bufferOffset = _serializer.float64(obj.velocity, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type TrajectoryRequest
    let len;
    let data = new TrajectoryRequest(null);
    // Deserialize message field [path_number]
    data.path_number = _deserializer.int64(buffer, bufferOffset);
    // Deserialize message field [seg_number]
    data.seg_number = _deserializer.int64(buffer, bufferOffset);
    // Deserialize message field [velocity]
    data.velocity = _deserializer.float64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 24;
  }

  static datatype() {
    // Returns string type for a service object
    return 'sensorfusion/TrajectoryRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'b53cf40120cba9795a639a4fe4e2a943';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    #Index of path (mini segments)
    int64 path_number
    
    #Index of segment (parts of trajectory)
    int64 seg_number
    
    #Linear velocity
    float64 velocity
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new TrajectoryRequest(null);
    if (msg.path_number !== undefined) {
      resolved.path_number = msg.path_number;
    }
    else {
      resolved.path_number = 0
    }

    if (msg.seg_number !== undefined) {
      resolved.seg_number = msg.seg_number;
    }
    else {
      resolved.seg_number = 0
    }

    if (msg.velocity !== undefined) {
      resolved.velocity = msg.velocity;
    }
    else {
      resolved.velocity = 0.0
    }

    return resolved;
    }
};

class TrajectoryResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.angle = null;
      this.dt = null;
      this.direction = null;
      this.seg_dt = null;
      this.seg_angle = null;
    }
    else {
      if (initObj.hasOwnProperty('angle')) {
        this.angle = initObj.angle
      }
      else {
        this.angle = 0.0;
      }
      if (initObj.hasOwnProperty('dt')) {
        this.dt = initObj.dt
      }
      else {
        this.dt = 0.0;
      }
      if (initObj.hasOwnProperty('direction')) {
        this.direction = initObj.direction
      }
      else {
        this.direction = 0.0;
      }
      if (initObj.hasOwnProperty('seg_dt')) {
        this.seg_dt = initObj.seg_dt
      }
      else {
        this.seg_dt = 0.0;
      }
      if (initObj.hasOwnProperty('seg_angle')) {
        this.seg_angle = initObj.seg_angle
      }
      else {
        this.seg_angle = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type TrajectoryResponse
    // Serialize message field [angle]
    bufferOffset = _serializer.float64(obj.angle, buffer, bufferOffset);
    // Serialize message field [dt]
    bufferOffset = _serializer.float64(obj.dt, buffer, bufferOffset);
    // Serialize message field [direction]
    bufferOffset = _serializer.float64(obj.direction, buffer, bufferOffset);
    // Serialize message field [seg_dt]
    bufferOffset = _serializer.float64(obj.seg_dt, buffer, bufferOffset);
    // Serialize message field [seg_angle]
    bufferOffset = _serializer.float64(obj.seg_angle, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type TrajectoryResponse
    let len;
    let data = new TrajectoryResponse(null);
    // Deserialize message field [angle]
    data.angle = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [dt]
    data.dt = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [direction]
    data.direction = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [seg_dt]
    data.seg_dt = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [seg_angle]
    data.seg_angle = _deserializer.float64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 40;
  }

  static datatype() {
    // Returns string type for a service object
    return 'sensorfusion/TrajectoryResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'b7e9557a3acbd3e210ab8668c813fde4';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    #Angle at which robot is heading
    float64 angle
    
    #Time to drive path
    float64 dt
    
    #Direction at which robot needs to drive
    float64 direction
    
    #Segment time to drive
    float64 seg_dt
    
    #Segment angle
    float64 seg_angle
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new TrajectoryResponse(null);
    if (msg.angle !== undefined) {
      resolved.angle = msg.angle;
    }
    else {
      resolved.angle = 0.0
    }

    if (msg.dt !== undefined) {
      resolved.dt = msg.dt;
    }
    else {
      resolved.dt = 0.0
    }

    if (msg.direction !== undefined) {
      resolved.direction = msg.direction;
    }
    else {
      resolved.direction = 0.0
    }

    if (msg.seg_dt !== undefined) {
      resolved.seg_dt = msg.seg_dt;
    }
    else {
      resolved.seg_dt = 0.0
    }

    if (msg.seg_angle !== undefined) {
      resolved.seg_angle = msg.seg_angle;
    }
    else {
      resolved.seg_angle = 0.0
    }

    return resolved;
    }
};

module.exports = {
  Request: TrajectoryRequest,
  Response: TrajectoryResponse,
  md5sum() { return 'ed5bcf930c479d7b46b85b6b7dafa672'; },
  datatype() { return 'sensorfusion/Trajectory'; }
};
