// Auto-generated. Do not edit!

// (in-package sensorfusion.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

let SyncPos = require('../msg/SyncPos.js');

//-----------------------------------------------------------

class MeasurementsRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
    }
    else {
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type MeasurementsRequest
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type MeasurementsRequest
    let len;
    let data = new MeasurementsRequest(null);
    return data;
  }

  static getMessageSize(object) {
    return 0;
  }

  static datatype() {
    // Returns string type for a service object
    return 'sensorfusion/MeasurementsRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'd41d8cd98f00b204e9800998ecf8427e';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    #Ask only for measurement data
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new MeasurementsRequest(null);
    return resolved;
    }
};

class MeasurementsResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.measure = null;
    }
    else {
      if (initObj.hasOwnProperty('measure')) {
        this.measure = initObj.measure
      }
      else {
        this.measure = new SyncPos();
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type MeasurementsResponse
    // Serialize message field [measure]
    bufferOffset = SyncPos.serialize(obj.measure, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type MeasurementsResponse
    let len;
    let data = new MeasurementsResponse(null);
    // Deserialize message field [measure]
    data.measure = SyncPos.deserialize(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += SyncPos.getMessageSize(object.measure);
    return length;
  }

  static datatype() {
    // Returns string type for a service object
    return 'sensorfusion/MeasurementsResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '6413914098d25bfa9253dc3582c7fffc';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    #Measurement data
    SyncPos measure
    
    ================================================================================
    MSG: sensorfusion/SyncPos
    std_msgs/Header header
    
    #Lidar 
    float64 x_pos_lid
    float64 y_pos_lid
    float64 x_var_lid
    float64 y_var_lid
    float64 azimuth_lid
    float64 azimuth_var_lid
    
    
    #Pixart Front 
    float64 x_pos_vpix
    float64 y_pos_vpix
    float64 x_var_vpix
    float64 y_var_vpix
    
    
    #Pixart Back
    float64 x_pos_hpix
    float64 y_pos_hpix
    float64 x_var_hpix
    float64 y_var_hpix
    
    
    #Qualisys
    float64 x_pos_qual
    float64 y_pos_qual
    float64 x_var_qual
    float64 y_var_qual
    
    # #IMU
    float64 azimuth_imu
    float64 azimuth_var_imu
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new MeasurementsResponse(null);
    if (msg.measure !== undefined) {
      resolved.measure = SyncPos.Resolve(msg.measure)
    }
    else {
      resolved.measure = new SyncPos()
    }

    return resolved;
    }
};

module.exports = {
  Request: MeasurementsRequest,
  Response: MeasurementsResponse,
  md5sum() { return '6413914098d25bfa9253dc3582c7fffc'; },
  datatype() { return 'sensorfusion/Measurements'; }
};
