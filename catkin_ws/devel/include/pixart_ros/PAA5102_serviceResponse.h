// Generated by gencpp from file pixart_ros/PAA5102_serviceResponse.msg
// DO NOT EDIT!


#ifndef PIXART_ROS_MESSAGE_PAA5102_SERVICERESPONSE_H
#define PIXART_ROS_MESSAGE_PAA5102_SERVICERESPONSE_H


#include <string>
#include <vector>
#include <memory>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace pixart_ros
{
template <class ContainerAllocator>
struct PAA5102_serviceResponse_
{
  typedef PAA5102_serviceResponse_<ContainerAllocator> Type;

  PAA5102_serviceResponse_()
    : values()  {
      values.assign(0.0);
  }
  PAA5102_serviceResponse_(const ContainerAllocator& _alloc)
    : values()  {
  (void)_alloc;
      values.assign(0.0);
  }



   typedef boost::array<float, 2>  _values_type;
  _values_type values;





  typedef boost::shared_ptr< ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator> const> ConstPtr;

}; // struct PAA5102_serviceResponse_

typedef ::pixart_ros::PAA5102_serviceResponse_<std::allocator<void> > PAA5102_serviceResponse;

typedef boost::shared_ptr< ::pixart_ros::PAA5102_serviceResponse > PAA5102_serviceResponsePtr;
typedef boost::shared_ptr< ::pixart_ros::PAA5102_serviceResponse const> PAA5102_serviceResponseConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator1> & lhs, const ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator2> & rhs)
{
  return lhs.values == rhs.values;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator1> & lhs, const ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace pixart_ros

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsMessage< ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator> >
{
  static const char* value()
  {
    return "c334281ada2d017b8a6955b9529fd69a";
  }

  static const char* value(const ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xc334281ada2d017bULL;
  static const uint64_t static_value2 = 0x8a6955b9529fd69aULL;
};

template<class ContainerAllocator>
struct DataType< ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator> >
{
  static const char* value()
  {
    return "pixart_ros/PAA5102_serviceResponse";
  }

  static const char* value(const ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator> >
{
  static const char* value()
  {
    return "float32[2] values\n"
"\n"
;
  }

  static const char* value(const ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.values);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct PAA5102_serviceResponse_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::pixart_ros::PAA5102_serviceResponse_<ContainerAllocator>& v)
  {
    s << indent << "values[]" << std::endl;
    for (size_t i = 0; i < v.values.size(); ++i)
    {
      s << indent << "  values[" << i << "]: ";
      Printer<float>::stream(s, indent + "  ", v.values[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // PIXART_ROS_MESSAGE_PAA5102_SERVICERESPONSE_H
