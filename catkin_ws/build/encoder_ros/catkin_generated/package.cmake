set(_CATKIN_CURRENT_PACKAGE "encoder_ros")
set(encoder_ros_VERSION "0.0.0")
set(encoder_ros_MAINTAINER "mrobot <mrobot@todo.todo>")
set(encoder_ros_PACKAGE_FORMAT "2")
set(encoder_ros_BUILD_DEPENDS "message_generation" "roscpp" "rospy" "std_msgs")
set(encoder_ros_BUILD_EXPORT_DEPENDS "message_generation" "roscpp" "rospy" "std_msgs")
set(encoder_ros_BUILDTOOL_DEPENDS "catkin")
set(encoder_ros_BUILDTOOL_EXPORT_DEPENDS )
set(encoder_ros_EXEC_DEPENDS "roscpp" "rospy" "std_msgs")
set(encoder_ros_RUN_DEPENDS "roscpp" "rospy" "std_msgs" "message_generation")
set(encoder_ros_TEST_DEPENDS )
set(encoder_ros_DOC_DEPENDS )
set(encoder_ros_URL_WEBSITE "")
set(encoder_ros_URL_BUGTRACKER "")
set(encoder_ros_URL_REPOSITORY "")
set(encoder_ros_DEPRECATED "")