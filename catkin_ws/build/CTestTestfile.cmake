# CMake generated Testfile for 
# Source directory: /home/mrobot/lattepanda3/mobile-robot/catkin_ws/src
# Build directory: /home/mrobot/lattepanda3/mobile-robot/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("encoder_ros")
subdirs("motion_ros")
subdirs("pixart_ros")
subdirs("qualisys")
subdirs("robot_ros")
subdirs("robot_localization")
subdirs("xsens_ros_mti_driver")
