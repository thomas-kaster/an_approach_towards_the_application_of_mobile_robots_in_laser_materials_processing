# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "sensorfusion: 3 messages, 2 services")

set(MSG_I_FLAGS "-Isensorfusion:/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg;-Istd_msgs:/opt/ros/noetic/share/std_msgs/cmake/../msg;-Igeometry_msgs:/opt/ros/noetic/share/geometry_msgs/cmake/../msg;-Isensor_msgs:/opt/ros/noetic/share/sensor_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(sensorfusion_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Position.msg" NAME_WE)
add_custom_target(_sensorfusion_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "sensorfusion" "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Position.msg" "std_msgs/Header"
)

get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/SyncPos.msg" NAME_WE)
add_custom_target(_sensorfusion_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "sensorfusion" "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/SyncPos.msg" "std_msgs/Header"
)

get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Motion.msg" NAME_WE)
add_custom_target(_sensorfusion_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "sensorfusion" "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Motion.msg" ""
)

get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Trajectory.srv" NAME_WE)
add_custom_target(_sensorfusion_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "sensorfusion" "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Trajectory.srv" ""
)

get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Measurements.srv" NAME_WE)
add_custom_target(_sensorfusion_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "sensorfusion" "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Measurements.srv" "sensorfusion/SyncPos:std_msgs/Header"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Position.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/sensorfusion
)
_generate_msg_cpp(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/SyncPos.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/sensorfusion
)
_generate_msg_cpp(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Motion.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/sensorfusion
)

### Generating Services
_generate_srv_cpp(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Trajectory.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/sensorfusion
)
_generate_srv_cpp(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Measurements.srv"
  "${MSG_I_FLAGS}"
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/SyncPos.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/sensorfusion
)

### Generating Module File
_generate_module_cpp(sensorfusion
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/sensorfusion
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(sensorfusion_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(sensorfusion_generate_messages sensorfusion_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Position.msg" NAME_WE)
add_dependencies(sensorfusion_generate_messages_cpp _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/SyncPos.msg" NAME_WE)
add_dependencies(sensorfusion_generate_messages_cpp _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Motion.msg" NAME_WE)
add_dependencies(sensorfusion_generate_messages_cpp _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Trajectory.srv" NAME_WE)
add_dependencies(sensorfusion_generate_messages_cpp _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Measurements.srv" NAME_WE)
add_dependencies(sensorfusion_generate_messages_cpp _sensorfusion_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(sensorfusion_gencpp)
add_dependencies(sensorfusion_gencpp sensorfusion_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS sensorfusion_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Position.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/sensorfusion
)
_generate_msg_eus(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/SyncPos.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/sensorfusion
)
_generate_msg_eus(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Motion.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/sensorfusion
)

### Generating Services
_generate_srv_eus(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Trajectory.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/sensorfusion
)
_generate_srv_eus(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Measurements.srv"
  "${MSG_I_FLAGS}"
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/SyncPos.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/sensorfusion
)

### Generating Module File
_generate_module_eus(sensorfusion
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/sensorfusion
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(sensorfusion_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(sensorfusion_generate_messages sensorfusion_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Position.msg" NAME_WE)
add_dependencies(sensorfusion_generate_messages_eus _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/SyncPos.msg" NAME_WE)
add_dependencies(sensorfusion_generate_messages_eus _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Motion.msg" NAME_WE)
add_dependencies(sensorfusion_generate_messages_eus _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Trajectory.srv" NAME_WE)
add_dependencies(sensorfusion_generate_messages_eus _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Measurements.srv" NAME_WE)
add_dependencies(sensorfusion_generate_messages_eus _sensorfusion_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(sensorfusion_geneus)
add_dependencies(sensorfusion_geneus sensorfusion_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS sensorfusion_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Position.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/sensorfusion
)
_generate_msg_lisp(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/SyncPos.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/sensorfusion
)
_generate_msg_lisp(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Motion.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/sensorfusion
)

### Generating Services
_generate_srv_lisp(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Trajectory.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/sensorfusion
)
_generate_srv_lisp(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Measurements.srv"
  "${MSG_I_FLAGS}"
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/SyncPos.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/sensorfusion
)

### Generating Module File
_generate_module_lisp(sensorfusion
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/sensorfusion
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(sensorfusion_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(sensorfusion_generate_messages sensorfusion_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Position.msg" NAME_WE)
add_dependencies(sensorfusion_generate_messages_lisp _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/SyncPos.msg" NAME_WE)
add_dependencies(sensorfusion_generate_messages_lisp _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Motion.msg" NAME_WE)
add_dependencies(sensorfusion_generate_messages_lisp _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Trajectory.srv" NAME_WE)
add_dependencies(sensorfusion_generate_messages_lisp _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Measurements.srv" NAME_WE)
add_dependencies(sensorfusion_generate_messages_lisp _sensorfusion_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(sensorfusion_genlisp)
add_dependencies(sensorfusion_genlisp sensorfusion_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS sensorfusion_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Position.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/sensorfusion
)
_generate_msg_nodejs(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/SyncPos.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/sensorfusion
)
_generate_msg_nodejs(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Motion.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/sensorfusion
)

### Generating Services
_generate_srv_nodejs(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Trajectory.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/sensorfusion
)
_generate_srv_nodejs(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Measurements.srv"
  "${MSG_I_FLAGS}"
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/SyncPos.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/sensorfusion
)

### Generating Module File
_generate_module_nodejs(sensorfusion
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/sensorfusion
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(sensorfusion_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(sensorfusion_generate_messages sensorfusion_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Position.msg" NAME_WE)
add_dependencies(sensorfusion_generate_messages_nodejs _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/SyncPos.msg" NAME_WE)
add_dependencies(sensorfusion_generate_messages_nodejs _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Motion.msg" NAME_WE)
add_dependencies(sensorfusion_generate_messages_nodejs _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Trajectory.srv" NAME_WE)
add_dependencies(sensorfusion_generate_messages_nodejs _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Measurements.srv" NAME_WE)
add_dependencies(sensorfusion_generate_messages_nodejs _sensorfusion_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(sensorfusion_gennodejs)
add_dependencies(sensorfusion_gennodejs sensorfusion_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS sensorfusion_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Position.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/sensorfusion
)
_generate_msg_py(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/SyncPos.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/sensorfusion
)
_generate_msg_py(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Motion.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/sensorfusion
)

### Generating Services
_generate_srv_py(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Trajectory.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/sensorfusion
)
_generate_srv_py(sensorfusion
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Measurements.srv"
  "${MSG_I_FLAGS}"
  "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/SyncPos.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/sensorfusion
)

### Generating Module File
_generate_module_py(sensorfusion
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/sensorfusion
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(sensorfusion_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(sensorfusion_generate_messages sensorfusion_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Position.msg" NAME_WE)
add_dependencies(sensorfusion_generate_messages_py _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/SyncPos.msg" NAME_WE)
add_dependencies(sensorfusion_generate_messages_py _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/msg/Motion.msg" NAME_WE)
add_dependencies(sensorfusion_generate_messages_py _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Trajectory.srv" NAME_WE)
add_dependencies(sensorfusion_generate_messages_py _sensorfusion_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/mrobot/lattepanda3/mobile-robot/catkin_ws/src/archived_code/breit_code/srv/Measurements.srv" NAME_WE)
add_dependencies(sensorfusion_generate_messages_py _sensorfusion_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(sensorfusion_genpy)
add_dependencies(sensorfusion_genpy sensorfusion_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS sensorfusion_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/sensorfusion)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/sensorfusion
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(sensorfusion_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()
if(TARGET geometry_msgs_generate_messages_cpp)
  add_dependencies(sensorfusion_generate_messages_cpp geometry_msgs_generate_messages_cpp)
endif()
if(TARGET sensor_msgs_generate_messages_cpp)
  add_dependencies(sensorfusion_generate_messages_cpp sensor_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/sensorfusion)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/sensorfusion
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(sensorfusion_generate_messages_eus std_msgs_generate_messages_eus)
endif()
if(TARGET geometry_msgs_generate_messages_eus)
  add_dependencies(sensorfusion_generate_messages_eus geometry_msgs_generate_messages_eus)
endif()
if(TARGET sensor_msgs_generate_messages_eus)
  add_dependencies(sensorfusion_generate_messages_eus sensor_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/sensorfusion)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/sensorfusion
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(sensorfusion_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()
if(TARGET geometry_msgs_generate_messages_lisp)
  add_dependencies(sensorfusion_generate_messages_lisp geometry_msgs_generate_messages_lisp)
endif()
if(TARGET sensor_msgs_generate_messages_lisp)
  add_dependencies(sensorfusion_generate_messages_lisp sensor_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/sensorfusion)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/sensorfusion
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(sensorfusion_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()
if(TARGET geometry_msgs_generate_messages_nodejs)
  add_dependencies(sensorfusion_generate_messages_nodejs geometry_msgs_generate_messages_nodejs)
endif()
if(TARGET sensor_msgs_generate_messages_nodejs)
  add_dependencies(sensorfusion_generate_messages_nodejs sensor_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/sensorfusion)
  install(CODE "execute_process(COMMAND \"/usr/bin/python3\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/sensorfusion\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/sensorfusion
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(sensorfusion_generate_messages_py std_msgs_generate_messages_py)
endif()
if(TARGET geometry_msgs_generate_messages_py)
  add_dependencies(sensorfusion_generate_messages_py geometry_msgs_generate_messages_py)
endif()
if(TARGET sensor_msgs_generate_messages_py)
  add_dependencies(sensorfusion_generate_messages_py sensor_msgs_generate_messages_py)
endif()
