#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#TODO adapt"""Panda Autodynamics package"""
#TODO DOCSTRING DOCUMENTATION
#TODO autoformat with black and isort
#TODO lint with flake8

from typing import Type
from nav_msgs.msg import Odometry
import rospy


def callback(values: Type[Odometry]) ->None:
    """Print sensor values.

    Args:
        values (Type[float]): Message from topic (PAA5102_values)
    """
    print(values)


def listener() -> None:
    """Initialize node and subscribe to topic 'PAA5102_values'"""
    rospy.init_node("pix_sub", anonymous=True)

    rospy.Subscriber("odom", Odometry, callback)

    rospy.spin()


if __name__ == "__main__":
    listener()
