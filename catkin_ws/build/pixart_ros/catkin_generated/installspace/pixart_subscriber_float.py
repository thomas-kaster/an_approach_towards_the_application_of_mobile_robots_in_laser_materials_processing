#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#TODO adapt"""Panda Autodynamics package"""
#TODO DOCSTRING DOCUMENTATION
#TODO autoformat with black and isort
#TODO lint with flake8

from typing import Type
from std_msgs.msg import Float64MultiArray
import rospy


def callback(values: Type[Float64MultiArray]) ->None:
    """Print sensor values.

    Args:
        values (Type[float]): Message from topic (PAA5102_values)
    """
    print(values.data)


def listener() -> None:
    """Initialize node and subscribe to topic 'PAA5102_values'"""
    rospy.init_node("PAA5102_sub", anonymous=True)

    rospy.Subscriber("PAA5102_values", Float64MultiArray, callback)

    rospy.spin()


if __name__ == "__main__":
    listener()
