#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#TODO adapt"""Panda Autodynamics package"""
#TODO DOCSTRING DOCUMENTATION
#TODO autoformat with black and isort
#TODO lint with flake8

import signal
import sys
import time
import numpy
import rospy
import serial
from nav_msgs.msg import Odometry
from typing import Type

sensor_adr = "/dev/ttyACM1"
baud_rate = 115200


def sensor_initialization() -> None:
    """Initialize sensor"""
    global sensor
    sensor = serial.Serial(sensor_adr, baud_rate, timeout=None)  # open serial port
    time.sleep(1)
    print(sensor.readline())
    sensor.write(b"pxi\n")
    print(sensor.readline())
    sensor.write(b"ver\n")
    print(sensor.readline())
    sensor.write(b"xy2uart_on\n")  # activate sensor output
    print(sensor.readline())


def time_to_float(rospy_time):
    rospy_time_float = float(rospy_time.secs) + float(rospy_time.nsecs) / 1e9
    return rospy_time_float


def sensor_readout() -> None:
    """Read sensor data and publish to topic 'odom'"""
    global sensor

    pub = rospy.Publisher("odom", Odometry, queue_size=1)
    rospy.init_node("pix_180", anonymous=True)
    msg = Odometry()
    cov = numpy.zeros(36)
    old_x, old_y,  = float(0.0), float(0.0)
    old_time = rospy.Time.now()

    while not rospy.is_shutdown():
        
        line = sensor.readline().strip().decode()
        values_string = numpy.array(line.split(";"))
        
        #Publishing sensor data
        msg.pose.pose.position.x = float(values_string[0])
        msg.pose.pose.position.y = float(values_string[1])
        msg.header.stamp = rospy.Time.now()
        msg.twist.twist.linear.x = (msg.pose.pose.position.x - old_x)/(time_to_float(rospy.Time.now())-time_to_float(old_time))
        msg.twist.twist.linear.y = (msg.pose.pose.position.y - old_y)/(time_to_float(rospy.Time.now())-time_to_float(old_time))
        msg.pose.covariance = cov
        msg.header.frame_id = "/pix_frame";  
        pub.publish(msg)

        old_x = msg.pose.pose.position.x
        old_y = msg.pose.pose.position.y
        old_time = msg.header.stamp

def exit(signal, frame) -> None:
    """Exit function that makes sure to close serial port.

    Args:
        signal (any): dummy
        frame (any): dummy
    """
    sensor.close()
    sensor.open()
    sensor.close()
    print("Sensor closed")
    sys.exit(0)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, exit)
    sensor_initialization()
    
    try:
        sensor_readout()
    except rospy.ROSInterruptException:
        pass