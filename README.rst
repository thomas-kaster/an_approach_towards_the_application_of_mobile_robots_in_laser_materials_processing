An approach towards the application of mobile robots in laser materials processing 
===========================================================

* `Supplementary Information <https://thomas-kaster.pages.git-ce.rwth-aachen.de/an_approach_towards_the_application_of_mobile_robots_in_laser_materials_processing/>`_
* `Repository <https://git-ce.rwth-aachen.de/thomas-kaster/an_approach_towards_the_application_of_mobile_robots_in_laser_materials_processing>`_

Features
--------

* Project is based on the method of agile product development `Schuh and Dölle 2021 <https://link.springer.com/book/10.1007/978-3-662-61910-0>`_

Video of mobile robot while performing laser welding tests
--------

.. raw:: html

    <embed>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/1ivXakn6fSE?si=kT9SQsIzX8jm7YXL" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </embed>


Acknowledgements
--------

* Funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) under Germanys Excellence Strategy – EXC-2023 Internet of Production – 390621612
* The authors acknowledge the financial support by the Federal Ministry of Education and Research of Germany in the framework of Research Campus Digital Photonic Production (project number: 13N15423)

.. image:: resources/IoP_Logo.png
.. image:: resources/Logo_Composing_DPP_BMBF_en.png
    

Data
--------

* The measurement data of the sensors (encoder, camera) and the reference measuring device (LaserTracker) can be found here: `Data <https://git-ce.rwth-aachen.de/thomas-kaster/an_approach_towards_the_application_of_mobile_robots_in_laser_materials_processing/-/tree/develop/Data>`_
